void load_data(String url){	
	String[] lines = loadStrings(url);
	for(String l :lines){
		if(l.startsWith("gene")){
			//header
		}else{
			String[] s = split(l, "\t");
			String gene_name = s[0].trim();
			String mut_name = s[1].trim();
			float mut_rep = Float.parseFloat(s[2]);
			float wt_rep = Float.parseFloat(s[3]);
			float mut_wt_con = Float.parseFloat(s[4]);
			String wt_name = s[5].trim();
			String cell_line = s[6].trim();
			String g_func = s[7].trim();
			String g_class = s[8].trim();

			// if(mut_name.contains("_WT")){
			// 	println("debug:ignore wild type:"+mut_name);
			// }else{
				//find cell
				Cell cell = (Cell)cell_map.get(cell_line);
				if(cell == null){
					cell = new Cell(cell_line);
					cell_map.put(cell_line, cell);
					all_cells.add(cell);
				}

				//find Gene for sorting
				Gene gene = (Gene)gene_map.get(gene_name);
				if(gene == null){
					gene = new Gene(gene_name, g_func, g_class);
					gene_map.put(gene_name, gene);
					all_genes.add(gene);

					if(!gene_functions.contains(g_func)){
						gene_functions.add(g_func);
					}
				}

				//within the cell line
				Gene g = (Gene)cell.gene_map.get(gene_name);
				if(g == null){
					g = new Gene(gene_name, wt_name, wt_rep);
					cell.gene_map.put(gene_name, g);
					cell.genes.add(g);
				}
				//add mutation
				Mutation mu = new Mutation(mut_name, mut_rep, mut_wt_con);
				g.mutations.add(mu);
			// }
		}
	}
}


//load IE information
void load_ie_list(String url){

	String[] lines = loadStrings(url);
	int[] indexes = new int[all_cells.size()];
	Arrays.fill(indexes, -1);
	for(String l : lines){
		if(l.startsWith("gene")){
			//header
			//find index of cell 
			String[] s = split(l, "\t");
			for(int i = 2; i < s.length; i++){
				String cell_line_name = s[i].trim();
				for(int j = 0; j < all_cells.size(); j++){
					if(cell_line_name.equals(all_cells.get(j).name)){
						indexes[j] = i;
					}
				}
			}
			println("debug: Array Index:"+Arrays.toString(indexes));
		}else{
			String[] s= split(l, "\t");
			String gene_name = s[0].trim();
			String mut_name = s[1].trim();
			
			//check if it is wild type
			boolean is_wt = false;
			String[] mut_name_array = split(mut_name, "_");
			if(mut_name_array.length >= 2){
				if(mut_name_array[1].startsWith("WT")){
					is_wt = true;
				}
			}	

			for(int i = 0; i < all_cells.size(); i++){
				if(indexes[i] != -1){
					Cell cell = all_cells.get(i);
					//get Gene
					Gene gene = (Gene) cell.gene_map.get(gene_name);
					if(gene != null){
						if(is_wt){
							if(gene.wt_ie == -1f){
								//store the wt ie
								float v = Float.parseFloat(s[indexes[i]]);
								gene.wt_ie = v;
							}
						}else{
							//find mutation
							Mutation mut = gene.findMutation(mut_name);
							if(mut != null){
								float v = Float.parseFloat(s[indexes[i]]);
								mut.ie = v;
								// println("debug: cell:"+cell.name+"\tgene:"+gene_name+"\tmut:"+mut_name+"\tv:"+v);
							}
						}
					}	
				}
			}
		}
	}
}

//load hotspot information
void load_hotspot(String url){
	//parse per gene
	for(Gene g: all_genes){
		String g_name = g.name;
		//check if file exist in the folder
		File f = new File(url+"/"+g_name+".txt");



		if(f.exists()){
			String[] lines = loadStrings(f);
			int last_pos = Integer.parseInt(split(lines[1], "\t")[1]);
			int total_count = Integer.parseInt(split(lines[2], "\t")[1]);
			//make histogram
			int[] hist = new int[last_pos+1];
			for(int i = 4; i < lines.length; i++){
				String[] s = split(lines[i], "\t");
				int pos = Integer.parseInt(s[0]);
				int count = Integer.parseInt(s[1]);
				hist[pos] = count;
			}

			//for each cell line
			for(Cell c:all_cells){
				//find Gene
				Gene gene = (Gene)c.gene_map.get(g_name);
				if(gene != null){
					//for each mutation, calculate score and determine if it is a hotspot
					for(Mutation m: gene.mutations){
						int pos = m.pos;
						//some mutations positions are not parsed
						if(pos != -1){
							int zero = hist[pos];
							int minus_2 = ((pos-2)>0)?hist[pos-2]:0;
							int minus_1 = ((pos-1)>0)?hist[pos-1]:0;
							int plus_1 = ((pos+1)<=last_pos)?hist[pos+1]:0;
							int plus_2 = ((pos+2)<=last_pos)?hist[pos+2]:0;

							//find max
							int max_count = 0;
							max_count = max(max_count, minus_2 + minus_1 + zero);
							max_count = max(max_count, minus_1 + zero + plus_1);
							max_count = max(max_count, zero + plus_1 +plus_2);
							float max_ratio = (float)max_count/(float)total_count;

							if(pos > 2){
								if(max_ratio >= 0.02f){
									m.is_hotspot = true;
								}
							}
						}
					}
				}
			}
		}else{
			println("debug **** no hotspot data for: "+g_name+"\t"+url+"/"+g_name+".txt");
		}
	}
}

//parse mutations
//assigny mutation type
//check the IE Difference
void parse_mutations(){
	for(int i = 0; i < all_cells.size(); i++){
		Cell c = all_cells.get(i);
		x_threshold = conf.robustness.get(i);
		y_threshold = conf.connectivity.get(i);
		
		// println("debug:"+i+" "+c.name);
		for(Gene g:c.genes){
			boolean is_wt_robust = false;
			if(g.wt_rep > x_threshold){
				is_wt_robust = true;
			}
			for(Mutation m:g.mutations){
				boolean is_mu_robust = false;
				boolean is_con = false;
				if(m.rep  > x_threshold){
					is_mu_robust = true;
				}
				if(m.con > y_threshold){
					is_con = true;
				}
				//{"GOF", "LOF", "SI", "NI"};
				if(is_wt_robust){
					if(is_mu_robust){
						if(is_con){
							m.type_index = 2; //silent
						}else{
							m.type_index = 0;
						}
					}else{
						if(is_con){
							m.type_index = 1;
						}else{
							m.type_index = 1;
						}
					}
				}else{
					if(is_mu_robust){
						if(is_con){
							m.type_index = 0;
						}else{
							m.type_index = 0;
						}
					}else{
						if(is_con){
							m.type_index = 3;
						}else{
							m.type_index = 3;
						}
					}
				}
			}
		}
	}	
	//check IE
	for(int i = 1; i < conf.cell_lines.size(); i++){
		String cell_line = conf.cell_lines.get(i);
		Cell cell = (Cell) cell_map.get(cell_line);
		if(cell != null){
			for(Gene gene:cell.genes){
				if(gene.wt_ie == -1f){
					//no wt data
					println("debug:"+gene.name+" has no wt ID data.");
				}else{
					float wt_ie = gene.wt_ie;
					//compare against all mutation
					for(Mutation mut: gene.mutations){
						float mut_ie = mut.ie;
						if(mut_ie == -1f){
							// println("debug:"+cell_line+ " "+ mut.name+" is missing IE data");
						}else{
							// println("debug:"+cell_line+ " "+ mut.name+" is matched IE data");
							float ratio = 0;
							if(wt_ie > mut_ie){
								ratio = wt_ie/mut_ie;
							}else{
								ratio = mut_ie/wt_ie;
							}
							if(ratio > 2){
								//flag
								mut.flag_ie = true;
								// println("flag mutation: "+mut+"  ratio="+ratio);
							}
						}
					}
				}
			}
		}
	}
}
