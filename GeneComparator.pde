class GeneComparator implements Comparator<Gene>{
	String type = "name";

	GeneComparator(String type){
		this.type = type;
	}

	public int compare(Gene gene_1, Gene gene_2){

		if(type.equals("name")){
			int result = gene_1.func.compareTo(gene_2.func);
			if(result != 0){
				return result;
			}else{
				return gene_1.name.compareTo(gene_2.name);
			}
		}else if(type.equals("class")){
			int result = gene_1.g_class.compareTo(gene_2.g_class);
			if(result != 0){
				return result;
			}else{
				return gene_1.name.compareTo(gene_2.name);
			}
		}
		// 
			// if(ind_1.colour > ind_2.colour){
			// 	return 1;
			// }else if( ind_1.colour < ind_2.colour){
			// 	return -1;
			// }else{
			// 	return ind_1.name.compareTo(ind_2.name);
			// }
		
		return 0;
	}
}
