class TextButton{
	String name;
	Rectangle area;
	String instruction;
	String text;
	boolean isSelected = false;


	TextButton(String name, int x, int y, int w, int h){
		this.name = name;
		this.area = new Rectangle(x, y, w, h);
		this.instruction = "";
		this.text = "";
		isSelected = false;
	}

	void render(){
		//draw box
		fill(180);
		noStroke();
		rect(area.x, area.y, area.width, area.height);
		//text
		fill(area.contains(mouseX, mouseY)?color_active:255);
		textAlign(CENTER, CENTER);
		text(instruction, (float) area.getCenterX(), (float) area.getCenterY());
	}
}