class Gene{
	float wt_rep = 0;
	float wt_con = 100;
	String func = "UNKN";
	String wt_name = "NA";
	String name = "NA";
	String g_class ="NA";


	float wt_ie = -1f;

	ArrayList<Mutation> mutations;
	Rectangle rect;
	Rectangle[] rects;

	float wt_dx, wt_dy; //display position

	Gene(String name, String wt_name, float wt_rep){
		this.name = name;
		this.wt_name = wt_name;
		this.wt_rep = wt_rep;
		this.mutations = new ArrayList<Mutation>();
	}

	//for sorting
	Gene(String name, String func){
		this.name = name;
		this.func = func;
	}
	Gene(String name, String func, String c){
		this.name = name;
		this.func = func;
		this.g_class = c;
	}

	

	void renderBackground(PGraphics pg, Rectangle r, int index){
		x_threshold = conf.robustness.get(index);
		y_threshold = conf.connectivity.get(index);

		//background
		pg.noStroke();
		pg.fill(255);
		pg.rect(r.x, r.y, r.width, r.height);
		//threshold values
		pg.stroke(240);
		pg.strokeCap(SQUARE);
		float tx = map(x_threshold, conf.x_min, conf.x_max, 0, conf.box_w) + r.x;
		float ty = map(y_threshold, conf.y_min, conf.y_max, conf.box_h, 0) + r.y;
		pg.strokeWeight(1);
		pg.line(tx, r.y, tx, r.y+r.height);
		pg.line(r.x, ty, r.x+r.width, ty);

		//x
		pg.textFont(small_font);
		pg.textAlign(CENTER, TOP);
		pg.fill(180);
		pg.text(round(x_threshold),tx, r.y+r.height);
		//y
		pg.textAlign(RIGHT, CENTER);
		pg.text(round(y_threshold),r.x -2, ty);
		pg.textFont(large_font);
		
		//outline
		pg.noFill();
		pg.stroke(160);
		pg.strokeWeight(1);
		pg.rect(r.x, r.y, r.width, r.height);

		//draw wt line
		
		float wt_y = r.y - _wt_gap;//map(100, conf.y_min, conf.y_max, conf.box_h, 0) + r.y;
		//wt line
		pg.stroke(160);
		pg.strokeWeight(1);
		pg.line(r.x, wt_y, r.x+r.width, wt_y);
		//tick marks
		pg.line(r.x, wt_y-2, r.x, wt_y+2);
		pg.line(r.x + r.width, wt_y-2, r.x+r.width, wt_y+2);
		pg.line(tx, wt_y-2, tx, wt_y+2);
	}


	//PGraphcis
	void renderPlot(PGraphics pg, Rectangle r, int opacity){
		//draw plot
		float wt_x = map(wt_rep, conf.x_min, conf.x_max, 0, conf.box_w) + r.x;
		float wt_y = r.y - _wt_gap;

		if(wt_x < r.x){
			wt_x = r.x - 2.5;
		}
		this.wt_dx = wt_x;
		this.wt_dy = wt_y;

		//middle line
		pg.strokeWeight(1);
		pg.stroke(180);
		pg.line(r.x, (float)r.getCenterY(), r.x+r.width, (float)r.getCenterY());


		//draw mutations
		for(Mutation m: mutations){
			float m_x = map(m.rep, conf.x_min, conf.x_max, 0, conf.box_w) + r.x;
			float m_y = map(m.con, conf.y_min, conf.y_max, conf.box_h, 0) + r.y;
			if(m_x < r.x){
				m_x = r.x - 2.5;
			}

			pg.stroke(colors[m.type_index], opacity);
			pg.strokeWeight(1);
			if(!m.flag_ie){
				pg.line(wt_x, wt_y, m_x, m_y);
			}else{
				dashline(pg, wt_x, wt_y, m_x, m_y, dash_line);
			}
			pg.fill(colors[m.type_index], opacity);
			pg.noStroke();
			pg.ellipse(m_x, m_y, 3, 3);

			//hot spot
			if(m.is_hotspot){
				pg.strokeWeight(0.5);
				pg.stroke(colors[m.type_index], opacity);
				pg.noFill();
				pg.ellipse(m_x, m_y, 5, 5);
			}


			//mutation label
			if(show_labels){
				pg.textFont(label_font);
				pg.textAlign(RIGHT, TOP);
				pg.fill(0, opacity);
				// pg.fill(colors[m.type_index]);
				String label = "???";
				if(m.name.contains("_")){
					label = split(m.name, "_")[1];
					// if(m.name.contains("WT")){
					// 	label = m.name;
					// }else{
					// }
				}else{
					label = m.name;
				}
				pg.text(label, m_x, m_y);
				pg.textFont(large_font);

				// println("debug: mutation name ="+m.name);
			}

			m.dx = m_x;
			m.dy = m_y;
		}


		//wt dot
		pg.fill(60);
		pg.noStroke();
		pg.ellipse(wt_x, wt_y, 3, 3);
		// pg.stroke(60);
		// pg.strokeWeight(2);
		// pg.strokeCap(ROUND);
		// pg.point(wt_x, wt_y);

		//outline
		if(wt_rep > x_threshold){
			pg.strokeWeight(2);
			pg.stroke(60);
			pg.noFill();
			pg.ellipse(wt_x, wt_y, 8, 8);
		}
	}

	

	Mutation findMutation(String name){
		for(Mutation m:mutations){
			if(m.name.equals(name)){
				return m;
			}
		}
		// println("debug: mismatch:"+name+":"+mutations);
		return null;
	}

	String toString(){
		return name;
	}


	//flag mutation 
	void flag_position(int pos){
		for(Mutation m: mutations){
			if(m.pos == pos){
				m.is_hotspot = true;
				// println("debug: hotspot:"+ this.name+"("+pos+") > "+m.name);
			}
		}
	}
}



void dashline(PGraphics p, float x0, float y0, float x1, float y1, float[] spacing) { 
  float distance = dist(x0, y0, x1, y1); 
  float [ ] xSpacing = new float[spacing.length]; 
  float [ ] ySpacing = new float[spacing.length]; 
  float drawn = 0.0;  // amount of distance drawn 
 
  if (distance > 0){ 
    int i; 
    boolean drawLine = true; // alternate between dashes and gaps 
 
    /* 
      Figure out x and y distances for each of the spacing values 
      I decided to trade memory for time; I'd rather allocate 
      a few dozen bytes than have to do a calculation every time 
      I draw. 
    */ 
    for (i = 0; i < spacing.length; i++) { 
      xSpacing[i] = lerp(0, (x1 - x0), spacing[i] / distance); 
      ySpacing[i] = lerp(0, (y1 - y0), spacing[i] / distance); 
    } 
 
    i = 0; 
    while (drawn < distance){ 
      if (drawLine){ 
       p.line(x0, y0, x0 + xSpacing[i], y0 + ySpacing[i]); 
      } 
      x0 += xSpacing[i]; 
      y0 += ySpacing[i]; 
      /* Add distance "drawn" by this line or gap */ 
      drawn = drawn + mag(xSpacing[i], ySpacing[i]); 
      i = (i + 1) % spacing.length;  // cycle through array 
      drawLine = !drawLine;  // switch between dash and gap 
    } 
  } 
} 

void dashline(float x0, float y0, float x1, float y1, float[] spacing) { 
  float distance = dist(x0, y0, x1, y1); 
  float [ ] xSpacing = new float[spacing.length]; 
  float [ ] ySpacing = new float[spacing.length]; 
  float drawn = 0.0;  // amount of distance drawn 
 
  if (distance > 0){ 
    int i; 
    boolean drawLine = true; // alternate between dashes and gaps 
 
    /* 
      Figure out x and y distances for each of the spacing values 
      I decided to trade memory for time; I'd rather allocate 
      a few dozen bytes than have to do a calculation every time 
      I draw. 
    */ 
    for (i = 0; i < spacing.length; i++) { 
      xSpacing[i] = lerp(0, (x1 - x0), spacing[i] / distance); 
      ySpacing[i] = lerp(0, (y1 - y0), spacing[i] / distance); 
    } 
 
    i = 0; 
    while (drawn < distance){ 
      if (drawLine){ 
        line(x0, y0, x0 + xSpacing[i], y0 + ySpacing[i]); 
      } 
      x0 += xSpacing[i]; 
      y0 += ySpacing[i]; 
      /* Add distance "drawn" by this line or gap */ 
      drawn = drawn + mag(xSpacing[i], ySpacing[i]); 
      i = (i + 1) % spacing.length;  // cycle through array 
      drawLine = !drawLine;  // switch between dash and gap 
    } 
  } 
}
