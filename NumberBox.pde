class NumberBox{
	int dx, dy, dw, dh;
	Rectangle num_rect, up_rect, down_rect;
	float v_current, v_max, v_min;
	String name;
	String instruction;

	int decimal = 0;
	int text_color = 60;
	int active_color = color(5,113,176);


	NumberBox(int dx, int dy, int dw, int dh, float v_current, float v_max, float v_min, String name){
		this.dx = dx;
		this.dy = dy;
		this.dw = dw;
		this.dh = dh;
		this.v_current = v_current;
		this.v_max = v_max;
		this.v_min = v_min;
		this.name = name;

		float btn_size = (float)ceil(dh /2);
		num_rect = new Rectangle(dx, dy, round(dw-btn_size), dh);
		up_rect = new Rectangle(round(dx+dw-btn_size), dy, round(btn_size), round(btn_size));
		down_rect = new Rectangle(round(dx+dw-btn_size), round(dy+dh-btn_size), round(btn_size), round(btn_size));
	}


	void render(){
		//instruction
		rectMode(CORNER);
		textAlign(LEFT, BOTTOM);
		fill(120);
		text(instruction, num_rect.x, num_rect.y);

		//draw box
		fill(255);
		stroke(180);
		strokeWeight(1);
		rect(num_rect.x, num_rect.y, num_rect.width, num_rect.height);
		rect(up_rect.x, up_rect.y, up_rect.width, up_rect.height);
		rect(down_rect.x, down_rect.y, down_rect.width, down_rect.height);
	
		// triangle
		if(up_rect.contains(mouseX, mouseY)){
			fill(color_active);
		}else{
			fill(180);
		}
		drawTraiange(up_rect, true);

		if(down_rect.contains(mouseX, mouseY)){
			fill(color_active);
		}else{
			fill(180);
		}
		drawTraiange(down_rect, false);

		fill(60);
		textAlign(RIGHT, CENTER);
		text(nf(v_current,0, decimal), num_rect.x+num_rect.width -2, (float)num_rect.getCenterY());
	}

	void drawTraiange(Rectangle rect, boolean isUP){
		// fill(240);
		int dif = 2;
		noStroke();
		if(isUP){
			triangle((float) rect.getCenterX(), rect.y+dif, rect.x+rect.width -dif, rect.y+rect.height-dif, rect.x+dif, rect.y+rect.height-dif);
		}else{
			triangle((float) rect.getCenterX(), rect.y+rect.height-dif, rect.x+rect.width -dif, rect.y+dif, rect.x+dif, rect.y+dif);
		}
	}

	boolean contains(int mx, int my){
		if(up_rect.x <= mx && mx <= up_rect.x+up_rect.width){
			if(up_rect.y <= my && my <= down_rect.y+down_rect.height ){
				return true;
			}
		}
		return false;
	}

	void evaluate(int mx, int my, float increment){
		if(up_rect.contains(mx, my)){
			increase(increment);
		}else if(down_rect.contains(mx, my)){
			decrease(increment);
		}
	}

	void increase(float n){
		this.v_current = min(v_current + n, v_max);
	}
	void decrease(float n){
		this.v_current = max(v_current - n, v_min);
	}

}
