class Mutation{
	float rep;
	float con;
	String name;

	int pos;
	boolean is_hotspot = false;

	float ie = -1;
	boolean flag_ie = false; //change if the difference is large

	// String type = "NI"; //GOF, LOF, SI(silent), NI(not informative)
	int type_index = 3; //NI

	float dx, dy;

	Mutation(String name, float rep, float con){
		this.name = name;
		this.rep = rep;
		this.con = con;

		//parse name to get position
		String s[] = split(this.name, ".");
		if(s.length >1){
			String label = s[1];
			String[] match = match(label, "([A-Z])(\\d+)([A-Z]|del)");
			if(match !=  null){
				// println("name ="+label+" >> "+Arrays.toString(match)+" >> "+match[2]);
				this.pos = Integer.parseInt(match[2]);
			}else{
				match = match(label, "(\\d+)([_])(\\S+)");
				if(match != null){
					// println("match:"+match[1]);
					this.pos = Integer.parseInt(match[1]);
				}else{
					match = match(label, "([A-Z])(\\d+)(\\D+)");
					if(match != null){
						// println("match:"+match[2]);
						this.pos = Integer.parseInt(match[2]);
					}else{
						println("\tdebug:"+this.name +" >> "+label);
					}
					
				}
			}
		}else if(name.contains("_")){
			s = split(this.name,  "_");
			if(s.length > 1){
				String label = s[1];
				String[] match = match(label, "([A-Z])(\\d+)([A-Z]|del)");
				if(match !=  null){
					// println("name ="+this.name+" >> "+Arrays.toString(match)+" >> "+match[2]);
					this.pos = Integer.parseInt(match[2]);
				}else{
					this.pos = -1;
				}
			}
		}else{
			this.pos = -1;	
		}

		if(pos == -1){
			println("error: unable to parse mutation position:"+name);
		}
	}

	String toString(){
		return name;
	}

}
