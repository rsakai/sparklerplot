class TextBox{
	String name;
	Rectangle area;
	String instruction;
	String text;
	Integer int_value = null;
	boolean isEditable = false;

	int active_color = color(5,113,176);


	TextBox(String name, int x, int y, int w, int h){
		this.name = name;
		this.area = new Rectangle(x, y, w, h);
		this.instruction = "";
		this.text = "";
	}

	void render(){
		//instruction
		textAlign(LEFT, BOTTOM);
		fill(120);
		text(instruction, area.x, area.y);

		//draw box
		fill(255);
		stroke(180);
		strokeWeight(1);
		rect(area.x, area.y, area.width, area.height);

		//underline
		if(isEditable){
			if(area.contains(mouseX, mouseY)){
				stroke(active_color);
				line(area.x, area.y+area.height, area.x+area.width, area.y+area.height);
			}
		}
	
		//selected file name
		fill(60);
		textAlign(LEFT, CENTER);
		text(text, area.x+4, (float)area.getCenterY());
	}

	void highlight(){
		stroke(active_color);
		strokeWeight(1);
		noFill();
		rect(area.x, area.y, area.width, area.height);
	}
}