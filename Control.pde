
void keyPressed(){
	if(!isFileSelected){

	}else{
		if(isFileLoaded){
			if(key == 'p'){
				//save pdf
				// println("save pdf");
				// record = true;
				// redraw();
				println("save pdf");
				//save pdf
				String path = timestamp()+".pdf";
				//determine export image size
				PGraphics pdf = createGraphics(stage_width, stage_height, PDF, path);
				pdf.beginDraw();
				pdf.smooth();
				update_display(pdf, 255);
				pdf.dispose();
				pdf.endDraw();
			}else if(key == 'l'){
				println("press l");
				//toggle labels
				show_labels  = !show_labels;
				update_display(p, 255);
				// update_display(p, transparency);
				loop();
			}
		}
	}
}



void mouseMoved(){
	if(!isFileSelected){
		cursor(ARROW);
		for(TextButton tb: textbtns){
			if(tb.area.contains(mouseX, mouseY)){
				cursor(HAND);
				loop();
				return;
			}
		}
		for(NumberBox nb: rob_numberboxes){
			if(nb.contains(mouseX, mouseY)){
				cursor(HAND);
				loop();
				return;
			}
		}
		for(NumberBox nb: con_numberboxes){
			if(nb.contains(mouseX, mouseY)){
				cursor(HAND);
				loop();
				return;
			}
		}

		loop();
	}else{
		cursor(ARROW);
		hovering_cell = null;
		hovering_gene = null;
		hovering_mutation = null;
		hovering_wt = false;
		hovering_gene_name = null;

		if(scroll_bar.contains(mouseX, mouseY)){
			cursor(HAND);
		}else if(mouseX < _left_margin){
			//check if it is hoving the name
			for(Rectangle r: gene_hit_area){
				if(r.contains(mouseX, mouseY-image_y_pos)){
					cursor(HAND);
					int m_y = mouseY - image_y_pos - _top_margin + _v_gap;
					int range = stage_height - _top_margin - _bottom_margin + _v_gap;
					int index = constrain(floor(map(m_y, abs(_top_margin-_v_gap), range, 0, n_row)), 0, all_genes.size()-1);
					hovering_gene_name = all_genes.get(index);
					return;
				}
			}
		}else{
			//find the current gene index from image_y_pos
			// int m_y = mouseY - image_y_pos - _top_margin;
			// int range = stage_height - _top_margin - _bottom_margin +_v_gap;
			// int index = constrain(floor(map(m_y, 0, range, 0, n_row)), 0, all_genes.size()-1);
			
			int m_y = mouseY - image_y_pos - _top_margin + _v_gap;
			int range = stage_height - _top_margin - _bottom_margin + _v_gap;
			int index = constrain(floor(map(m_y, abs(_top_margin-_v_gap), range, 0, n_row)), 0, all_genes.size()-1);

			int mouse_y = mouseY - image_y_pos;

			Gene gene = all_genes.get(index);
			//check if cursor in the box
			outerloop:
			for(int i = 0; i< gene.rects.length; i++){
				Rectangle r = gene.rects[i];
				if(r.x - _margin < mouseX && mouseX < r.x+r.width+_margin && mouse_y < r.y+r.height){
					//hit
					Cell cell = all_cells.get(i);
					//check if the gene exist
					Gene ge = (Gene) cell.gene_map.get(gene.name);
					if(ge != null){
						// println("debug:"+gene.name+" of "+ cell.name + "  m_y="+m_y+"  mouseY="+mouseY);
						cursor(CROSS);

						float closest = 3f;
						Mutation closest_m = null;
						for(Mutation m : ge.mutations){
							float dist = dist(mouseX, mouse_y, m.dx, m.dy);
							if(dist < closest){
								closest_m = m;
								closest = dist;
							}
						}
						if(closest_m != null){
							hovering_cell = cell;
							hovering_gene = ge;
							hovering_mutation = closest_m;
							cursor(target_cursor);
							println("debug:"+ge.name+" of "+ cell.name +"  "+ closest_m.name);
							break outerloop;
						}else{
							//check hovering wt
							float dist = dist(mouseX, mouse_y, ge.wt_dx, ge.wt_dy);
							if(dist < closest){
								hovering_cell = cell;
								hovering_gene = ge;
								hovering_wt = true;
								cursor(target_cursor);
								break outerloop;
							}
						}
					}
					break outerloop;
				}
			}
		}
		loop();
	}
	
}

void mousePressed(){
	if(!isFileSelected){
		for(TextButton tb: textbtns){
			if(tb.area.contains(mouseX, mouseY)){
				if(tb.name.equals("input_btn")){
					//select input file
					selectInput("Select an input file:", "select_input_file");
				}else if(tb.name.equals("hotspot_btn")){
					//select hotspot directory
					selectFolder("Select the hotspot file directory:", "select_hotspot_folder");
				}else if(tb.name.equals("ie_btn")){
					//select ie file
					selectInput("Select an Infection Efficiency file:", "select_ie_file");
				}else if(tb.name.equals("conf_load_btn")){
					//load configuration
					selectInput("Select a configuration file:", "select_conf_load");

				}else if(tb.name.equals("conf_save_btn")){
					selectFolder("Select a folder to save the configuration file:", "select_conf_save");
				}else if(tb.name.equals("load_btn")){
					//check if the configuration is ready to load
					if(conf.isReady()){
						//start loading
						println("debug: it is ready to load!");
						start_loading();
					}else{
						//pop up message
						println("debug: it is NOT ready to load!\n"+conf.msg);
						try {
							//pop up
							javax.swing.JOptionPane.showMessageDialog(null, "Some errors with the current configuration:\n"+conf.msg);
						} catch (Exception e) {
						   println("error on load_btn click:"+e);
						}

					}
				}
			}
		}
		//number box
		for(NumberBox nb: rob_numberboxes){
			if(nb.contains(mouseX, mouseY)){
				nb.evaluate(mouseX, mouseY, 1.0f);
				loop();
				return;
			}
		}
		for(NumberBox nb: con_numberboxes){
			if(nb.contains(mouseX, mouseY)){
				nb.evaluate(mouseX, mouseY, 1.0f);
				loop();
				return;
			}
		}		
	}else{
		if(scroll_bar.contains(mouseX, mouseY)){
			isScrolling = true;
			m_pressed_y = mouseY;
		}

		if(hovering_gene_name != null){
			if(include_hotspot){
				createLollypop(hovering_gene_name.name);
			}
		}
	}
}

void mouseDragged(){
	if(!isFileSelected){
	}else{
		if(isScrolling){
			int diff = mouseY  - m_pressed_y; //how many pixel it moved
			int prev_bar_y = scroll_bar.y;
			int new_bar_y = constrain(prev_bar_y + diff, scroll_area.y, scroll_area.y+scroll_area.height-scroll_bar.height);
			scroll_bar.setLocation(scroll_bar.x, new_bar_y);
			m_pressed_y = mouseY;
			//update image position
			image_y_pos = -round(map(new_bar_y, scroll_area.y, scroll_area.y+scroll_area.height, 0, stage_height));
			
			loop();
		}
	}
}

void mouseReleased(){
	if(!isFileSelected){
	}else{
		isScrolling = false;
		hovering_gene_name = null;
		loop();
	}
}
