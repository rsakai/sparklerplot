//Lollypop
class Pop implements Comparable<Pop>{
	ArrayList<Mutation> mutations;
	ArrayList<String> cells;
	String gene_name;
	String mut_name;
	int pos;

	Rectangle rect;

	float sx, sy, sx2, sy2, sx3, sy3; //stick position
	float bx, by; //box position

	Pop(String gene_name, String mut_name){
		this.gene_name = gene_name;
		this.mut_name = mut_name;
		this.mutations = new ArrayList<Mutation>();
		this.cells = new ArrayList<String>();
	}

	public int compareTo(Pop that){
		if(this.pos < that.pos){
			return -1;
		}else if(this.pos > that.pos){
			return 1;
		}else{
			return 0;
		}
	}
}
