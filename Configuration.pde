class Configuration{
	int box_w = _margin*10;
	int box_h = _margin*15;
	String x_axis_label = "signature robustness";
	int[] x_axis_ticks = {0, 100};
	String y_axis_label = "MUT connectivity to WT(MI)";
	int[] y_axis_ticks = {-100, 0, 100};
	int x_min = 0;
	int x_max = 105;
	int y_min = -105;
	int y_max = 105;

	String input_file_url;
	String hotspot_folder_url;
	String ie_file_url;
	String local_url;

	ArrayList<String> cell_lines; 
	ArrayList<Integer> robustness;
	ArrayList<Integer> connectivity;

	String msg = "";
	boolean isError = false;

	//default constructor
	Configuration(){
		input_file_url = "";
		hotspot_folder_url = "";
		ie_file_url = "";
		local_url = "";
		cell_lines = new ArrayList<String>();
		robustness = new ArrayList<Integer>();
		connectivity = new ArrayList<Integer>();
	}

	Configuration(String[] lines, String local){
		input_file_url = "";
		hotspot_folder_url = "";
		ie_file_url = "";
		local_url = local;
		cell_lines = new ArrayList<String>();
		robustness = new ArrayList<Integer>();
		connectivity = new ArrayList<Integer>();
		for(String l: lines){
			if(l.trim().length() > 0){
				String[] s = split(l, "\t");
				if(l.startsWith("#")){
					if(s[0].trim().toLowerCase().equals("#box_w")){
						box_w = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#box_h")){
						box_h = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#x_axis")){
						x_axis_label = s[1].trim();
						ArrayList<Integer> ticks = new ArrayList<Integer>();
						for(int i = 2; i < s.length; i++){
							if(!s[i].trim().equals("")){
								int tick = Integer.parseInt(s[i]);
								ticks.add(tick);
							}
						}
						x_axis_ticks = new int[ticks.size()];
						for(int i = 0; i< ticks.size(); i++){
							x_axis_ticks[i] = ticks.get(i).intValue();
						}
						
					}else if(s[0].trim().toLowerCase().equals("#y_axis")){
						y_axis_label = s[1].trim();
						ArrayList<Integer> ticks = new ArrayList<Integer>();
						for(int i = 2; i < s.length; i++){
							if(!s[i].trim().equals("")){
								int tick = Integer.parseInt(s[i]);
								ticks.add(tick);
							}
						}
						y_axis_ticks = new int[ticks.size()];
						for(int i = 0; i< ticks.size(); i++){
							y_axis_ticks[i] = ticks.get(i).intValue();
						}
					}else if(s[0].trim().toLowerCase().equals("#x_min")){
						x_min = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#x_max")){
						x_max = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#y_min")){
						y_min = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#y_max")){
						y_max = Integer.parseInt(s[1]);
					}
				}else{
					if(s[0].trim().toLowerCase().equals("input")){
						input_file_url = s[1].trim();
						//check if file exist
						File f = new File(this.input_file_url);
						if(f.exists()){
							//exist
							select_input_file(f);
						}else{
							//check in data folder
							f = new File(dataPath(this.input_file_url));
							if(f.exists()){
								//exists
								input_file_url = dataPath(this.input_file_url);
								select_input_file(f);
							}else{
								//check in relative folder
								f = new File(local_url+"/"+input_file_url);
								if(f.exists()){
									//file is in the relative urls
									input_file_url = local_url+"/"+input_file_url;
									println("debug:loading relative path:"+input_file_url);
									select_input_file(f);
								}else{
									println("debug:loading relative path was not found:"+local_url+"/"+input_file_url);
									f = null;
									isError = true;
									if(this.input_file_url.equals("")){
										msg += "The input file url is not defined.\t";
									}else{
										msg += "The specified file ("+this.input_file_url+") was not found. \n";
									}
								}
							}
						}
					}else if(s[0].trim().toLowerCase().equals("hotspot")){
						hotspot_folder_url = s[1].trim();
						//check if file exist
						File f = new File(hotspot_folder_url);
						if(f.exists()){
							//exist

						}else{
							//check in data folder
							f = new File(dataPath(hotspot_folder_url));
							if(f.exists()){
								//exists
								hotspot_folder_url = dataPath(hotspot_folder_url);
							}else{
								if(hotspot_folder_url.equals("")){
									//not defined
								}else{
									f = new File(local_url+"/"+hotspot_folder_url);
									if(f.exists()){
										println("debug: found hotspot folder:"+f.getAbsolutePath());
										hotspot_folder_url = local_url+"/"+hotspot_folder_url;
									}else{
										f = null;
										isError = true;
										msg += "The specified folder ("+hotspot_folder_url+") was not found. \n";
									}
								}
							}
						}
						if(f == null){
							hotspot_file = null;
						}else{
							if(f.isDirectory()){
								select_hotspot_folder(f);
							}else{
								f = null;
								isError = true;
								msg += "The specified url ("+hotspot_folder_url+") is not a directory. \n";
							}
						}
					}else if(s[0].trim().toLowerCase().equals("ie")){
						ie_file_url = s[1].trim();
						//check if file exist
						File f = new File(ie_file_url);
						if(f.exists()){
							//exist
						}else{
							//check in data folder
							f = new File(dataPath(ie_file_url));
							if(f.exists()){
								//exists
							}else{
								if(ie_file_url.equals("")){
									//not defined									
								}else{
									f = new File(local_url+"/"+ie_file_url);
									if(f.exists()){
										ie_file_url = local_url+"/"+ie_file_url;
									}else{
										f = null;
										isError = true;
										msg += "The specified file ("+ie_file_url+") was not found. \n";
									}
								}
							}
						}
						if(f == null){
							ie_file = null;
						}else{
							select_ie_file(f);
						}
					}else if(s[0].trim().toLowerCase().equals("cell")){
						// celll line
						String name = s[1].trim();
						int c_rob = Integer.parseInt(s[2]);
						int c_con = Integer.parseInt(s[3]);

						cell_lines.add(name);
						robustness.add(c_rob);
						connectivity.add(c_con);
					}
				}
			}
		}
		// println("debug: new configuration: \n"+exportConfiguration());
	}

	String exportConfiguration(){
		String output = "";
		output += "#box_width\t"+box_w+"\n";
		output += "#box_height\t"+box_h+"\n";
		output += "#x_axis\t"+x_axis_label;
		for(int i :x_axis_ticks){
			output+= "\t"+i;
		}
		output += "\n";
		output += "#y_axis\t"+y_axis_label;
		for(int i : y_axis_ticks){
			output += "\t"+i;
		}
		output += "\n";
		output += "#x_min\t"+x_min+"\n";
		output += "#x_max\t"+x_max+"\n";
		output += "#y_min\t"+y_min+"\n";
		output += "#y_max\t"+y_max+"\n";


		output += "input\t"+input_file_url+"\n";
		output += "hotspot\t"+hotspot_folder_url+"\n";
		output += "ie\t"+ie_file_url+"\n";

		for(int i = 0; i< cell_lines.size(); i++){
			output += "cell\t"+cell_lines.get(i)+"\t"+robustness.get(i)+"\t"+connectivity.get(i)+"\n";
		}
		return output;
	}


	boolean isReady(){
		msg = "";
		isError = false;
		include_hotspot = false;
		include_IE = false;
		//check file existance
		File f = new File(dataPath(input_file_url));
		if(!f.exists()){
			f = new File(input_file_url);
			if(!f.exists()){
				isError = true;
				msg += "the input file was not found. \n";
			}
		}else{
			input_file_url = dataPath(input_file_url);
		}
		if(!hotspot_folder_url.equals("")){
			f = new File(dataPath(hotspot_folder_url));
			if(!f.exists()){
				f = new File(hotspot_folder_url);
				if(!f.exists()){
					isError = true;
					msg += "the hotspot data folder was not found. \n";
				}else{
					include_hotspot = true;
				}
			}else{
				hotspot_folder_url = dataPath(hotspot_folder_url);
				include_hotspot = true;
			}
		}
		if(!ie_file_url.equals("")){
			f = new File(dataPath(ie_file_url));
			if(!f.exists()){
				f = new File(ie_file_url);
				if(!f.exists()){
					isError = true;
					msg += "the ie data file was not found. "+ie_file_url+" \n";
				}else{
					include_IE = true;
				}
			}else{
				ie_file_url = dataPath(ie_file_url);
				include_IE = true;
			}
		}
		return isError?false:true;

	}


	boolean isValidURL(String filename){
		if (filename.contains(":")) {  // at least smells like URL
			try {
				URL url = new URL(filename);
				InputStream stream = url.openStream();
				return true;
			} catch (MalformedURLException mfue) {
				// not a url, that's fine
				return false;
			} catch (IOException e) {
				// changed for 0117, shouldn't be throwing exception
				e.printStackTrace();
				//System.err.println("Error downloading from URL " + filename);
				return false;
			}
	    }
	    return false;
	}

}