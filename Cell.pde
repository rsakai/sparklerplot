class Cell{
	String name;
	ArrayList<Gene> genes;
	HashMap<String, Gene> gene_map;

	Cell(String name){
		this.name = name;
		this.genes = new ArrayList<Gene>();
		this.gene_map = new HashMap<String, Gene>();
	}

	String toString(){
		return name+"("+genes.size()+")";
	}

	
}
