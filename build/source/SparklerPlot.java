import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.awt.event.*; 
import java.awt.*; 
import java.util.*; 
import javax.swing.*; 
import processing.pdf.*; 
import java.net.*; 
import java.io.FileNotFoundException; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class SparklerPlot extends PApplet {













//display
int _margin = 10;
int _top_margin = _margin*5;
int _bottom_margin = _margin *5;
int _left_margin = _margin*10;
int _right_margin = _margin*7;
int _banner_height = 50;
int _h_gap = _margin*2;
int _v_gap = _margin*5; 
int _tick = 2;

int _wt_gap = _margin;

int stage_height, stage_width;

int n_row = 5;
int n_col = 5; // number of cell line

Rectangle legend_rect;


//data
HashMap<String, Gene> gene_map;
ArrayList<Gene> all_genes;
HashMap<String, Cell> cell_map;
ArrayList<Cell> all_cells;

ArrayList<String> gene_functions;

//dashed line 
float[] dash_line = {1f, 2f};

// //threshod
float x_threshold = 73f;
float y_threshold = 90f;


PFont font; 
PFont large_font;// = createFont("SansSerif", 12);
PFont small_font;// = createFont("SansSerif", 8);
PFont label_font;// = createFont("pf_tempesta_seven.ttf", 7);

//flags
boolean record = false;
boolean show_labels = false;
//stages
boolean isFileSelected = false;
boolean isFileLoading = false;
boolean isFileLoaded = false;
//data
boolean include_IE = false;
boolean include_hotspot = false;


String[] mu_types = {"GOF", "LOF", "Silence", "NotInformative"};

//scroll bar
Rectangle scroll_area, scroll_bar, scroll_bg;
int image_y_pos = 0;
boolean isScrolling = false;
int m_pressed_y;

//color coding
int c_red = color(202,0,32);
int c_blue = color(5,113,176);
int c_light_gray = 200;
int c_dark_Gray = 60;
int[] colors = {c_red, c_blue, c_dark_Gray, c_light_gray};


PGraphics p;
PGraphics p_trans;
int transparency = 20;

PImage target_cursor;
Cell hovering_cell = null;
Gene hovering_gene = null;
Mutation hovering_mutation = null;
boolean hovering_wt = false;


int draw_counter = 0;
int draw_max = 60; //2 seconds = draw 60 times before it stops


//Lollypop
float pos_weight = 0.5f;
int lolly_stick_length = _margin*4;

//gene name hit area
Rectangle[] gene_hit_area;
Gene hovering_gene_name = null;

//Configuration
Configuration conf;


public void setup(){
	stage_width = displayWidth;
	stage_height = displayHeight - _banner_height;
	size(stage_width, stage_height);
	//font
	font = createFont("SansSerif", 12);
	large_font = createFont("SansSerif", 12);
	small_font = createFont("SansSerif", 8);
	label_font = createFont("SansSerif", 10);
	textFont(font);

	//set up configuration
	conf = new Configuration();


	//data loading page
	setup_data_loading_page();
}



public void start_loading(){
	isFileSelected = true;
	isFileLoading = true;
	//load data
	cell_map = new HashMap<String, Cell>();
	gene_map = new HashMap<String, Gene>();
	all_genes = new ArrayList<Gene>();
	gene_functions = new ArrayList<String>();
	all_cells = new ArrayList<Cell>();

	load_data(conf.input_file_url);

	//Infection Efficiency data
	if(include_IE){
		println("loading IE file...");
		load_ie_list(conf.ie_file_url);
	}

	//hotspot information
	if(include_hotspot){
		println("loading Hotspot file...");
		load_hotspot(conf.hotspot_folder_url);
	}

	//cursor
	target_cursor = loadImage("cursor.png");


	println("total number of genes = "+all_genes.size());
	println("gene functions = "+gene_functions);
	println("cell lines = "+ all_cells);

	n_col = all_cells.size();
	//sort genes
	// Collections.sort(all_genes, new GeneComparator("name"));
	Collections.sort(all_genes, new GeneComparator("class"));
	
	//parse mutation
	parse_mutations();
	//setup display
	setup_display();

	//draw
	update_display(p, 255);
	update_display(p_trans, transparency);

	smooth();
	noLoop();
	frameRate(25);

	//end of loading
	isFileLoading = false;
	isFileLoaded = true;
}


public void setup_display(){
	//stage size
	n_row = all_genes.size();
	stage_width = _left_margin + _right_margin + n_col*conf.box_w + (n_col-1)*_h_gap;
	stage_height = _top_margin + _bottom_margin + n_row*conf.box_h + (n_row-1)*_v_gap;
	size(stage_width, displayHeight - _banner_height);
	println(stage_width +" x "+ stage_height);

	p = createGraphics(stage_width, stage_height);
	p_trans = createGraphics(stage_width, stage_height);
	
	//adjust frame size
	frame.setResizable(true);
	frame.setSize(stage_width, displayHeight-_banner_height);



	//set rectangle for each gene
	int row_counter = 0;
	int col_counter = 0;
	int runningY = _top_margin;

	//gene name hit area
	gene_hit_area = new Rectangle[all_genes.size()];
	int counter = 0;
	for(Gene g: all_genes){
		g.rects = new Rectangle[n_col];
		int runningX = _left_margin;
		//name area
		gene_hit_area[counter] = new Rectangle(0, runningY - _wt_gap, _left_margin, 15);

		for(int i = 0; i < n_col; i++){
			g.rects[i] = new Rectangle(runningX, runningY, conf.box_w, conf.box_h);
			runningX += conf.box_w + _h_gap;
		}
		runningY += conf.box_h + _v_gap;
		counter ++;
	}
	int runningX = _left_margin;
	//legend
	legend_rect = new Rectangle(runningX, runningY, conf.box_w, conf.box_h);

	//scroll bar
	scroll_bg = new Rectangle(stage_width - _margin, 0, _margin, (displayHeight-_banner_height));
	scroll_area = new Rectangle(scroll_bg.x, scroll_bg.y+_margin/2, _margin, (displayHeight-_banner_height)-_margin);
	int bar_height = round(map((displayHeight-_banner_height), 0, stage_height, 0, scroll_area.height));
	int bar_y_1 = _margin/2;
	scroll_bar = new Rectangle(scroll_area.x, scroll_area.y, scroll_area.width, bar_height);
	image_y_pos = 0;
}

public void update_main_view(Pop p){
	if(p== null){
		hovering_mutation = null;
		hovering_gene = null;
	}else{
		hovering_mutation = p.mutations.get(0);
		String cell_name = p.cells.get(0);
		Cell c = (Cell) cell_map.get(cell_name);
		hovering_gene = (Gene) c.gene_map.get(p.gene_name);
	}
	loop();
}

public void draw(){
	if(record){
		beginRecord(PDF, timestamp()+".pdf");
	}
	background(240);

	if(!isFileSelected){
		//draw data loading page
		draw_data_loading_page();
	}else{
		if(isFileLoading){
			//while data is loading
			textAlign(CENTER, CENTER);
			fill(120);
			text("Loading data ... ", displayWidth/2, displayHeight/2);
		}else if(isFileLoaded){
			// image(p, 0, image_y_pos);
			if(hovering_mutation == null){
				image(p, 0, image_y_pos);
			}else{
				image(p_trans, 0, image_y_pos);
			}

			//draw hovering item
			draw_interaction();
			draw_scroll_bar();
		}
	}
	if(record){
		endRecord();
		record = false;
	}

	if(draw_counter > draw_max){
		println(" ---------");
		noLoop();
		draw_counter = 0;
	}else{
		draw_counter ++;
	}

}



//draw hovering item
public void draw_interaction(){
	if(hovering_gene != null){
		Gene hg = hovering_gene;
		if(hovering_wt){
			//hovering wt 
			// println("debug: hovering wt");
		}else{
			if(hovering_mutation != null){
				Mutation hm = hovering_mutation;
				//high light hovering mutation
				float wt_x = hg.wt_dx;
				float wt_y = hg.wt_dy + image_y_pos;
				float m_x = hm.dx;
				float m_y = hm.dy + image_y_pos;

				stroke(colors[hm.type_index]);
				strokeWeight(1);
				if(!hm.flag_ie){
					line(wt_x, wt_y, m_x, m_y);
				}else{
					dashline(wt_x, wt_y, m_x, m_y, dash_line);
				}
				fill(colors[hm.type_index]);
				noStroke();
				ellipse(m_x, m_y, 3, 3);
				//hot spot
				if(hm.is_hotspot){
					strokeWeight(0.5f);
					stroke(colors[hm.type_index]);
					noFill();
					ellipse(m_x, m_y, 5, 5);
				}
				


				//draw label
				textFont(label_font);
				textAlign(CENTER, CENTER);
				String label = "  "+split(hm.name, "_")[1]+"  ";
				float textWidth = textWidth(label);
				float textHeight = 15;
				fill(255, 180);
				noStroke();
				rect(m_x, m_y, textWidth, textHeight);
				fill(0);
				text(label, m_x + textWidth/2, m_y+textHeight/2);



				//highlight mutations on other cell line
				for(Cell c: all_cells){
					if(c != hovering_cell){
						Gene g = (Gene)c.gene_map.get(hg.name);
						if(g != null){
							//check if the mutation exist
							String m_name = hm.name;
							Mutation m = g.findMutation(m_name);
							if(m != null){
								//draw these lines
								float wt_x_2 = g.wt_dx;
								float wt_y_2 = g.wt_dy + image_y_pos;
								float m_x_2 = m.dx;
								float m_y_2 = m.dy + image_y_pos;
								stroke(colors[m.type_index]);
								strokeWeight(1);
								if(!m.flag_ie){
									line(wt_x_2, wt_y_2, m_x_2, m_y_2);
								}else{
									dashline(wt_x_2, wt_y_2, m_x_2, m_y_2, dash_line);
								}

								fill(colors[m.type_index]);
								noStroke();
								ellipse(m_x_2, m_y_2, 3, 3);
								//hot spot
								if(m.is_hotspot){
									strokeWeight(0.5f);
									stroke(colors[m.type_index]);
									noFill();
									ellipse(m_x_2, m_y_2, 5, 5);
								}

								//draw label
								textFont(label_font);
								textAlign(CENTER, CENTER);
								// fill(255, 120);
								// noStroke();
								// rect(m_x_2, m_y_2, textWidth, textHeight);
								fill(0);
								text(label, m_x_2 + textWidth/2, m_y_2+textHeight/2);

							}
						}
					}
				}
			}else{
				// println("debug: mutation is null");
			}
		}
	}else{
		// println("\tdebug: hovering_gene is null");
	}

	if(hovering_gene_name != null){
		//highlight gene name
		fill(c_red);
		Rectangle rect = hovering_gene_name.rects[0];
		textFont(large_font);
		textAlign(RIGHT, TOP);
		text(hovering_gene_name.name, rect.x-_margin*2.5f, rect.y - _wt_gap);

	}
}

public void draw_scroll_bar(){
	strokeCap(ROUND);
	strokeWeight(_margin);
	if(scroll_bar.contains(mouseX, mouseY)){
		stroke(120);
	}else{
		stroke(180);
	}
	line((float)scroll_bar.getCenterX(), scroll_bar.y, (float)scroll_bar.getCenterX(), scroll_bar.y+scroll_bar.height);
}

//update PGraphics
public void update_display(PGraphics pg, int opacity){
	pg.beginDraw();
	pg.smooth();
	pg.background(240);
	pg.textFont(large_font);

	float runningX = _left_margin;
	for(int i = 0; i < all_cells.size(); i++){
		Cell c = all_cells.get(i);
		//draw cell line name
		Gene first_g = all_genes.get(0);
		String label = split(c.name, "_")[0];
		pg.fill(120);
		pg.textAlign(CENTER, TOP);
		pg.text(label, (float)first_g.rects[i].getCenterX(), _margin*2);

		for(int k = 0; k < all_genes.size(); k++){
			Gene g = all_genes.get(k);
			Rectangle rect = g.rects[i];
			Gene gene = (Gene) c.gene_map.get(g.name);
			if(gene != null){
				gene.renderBackground(pg, rect, i);
				gene.renderPlot(pg, rect, opacity);
			}

			//gene label
			if(i == 0 ){
				pg.textAlign(RIGHT, TOP);
				//gene name
				pg.fill(0);
				pg.text(g.name, rect.x-_margin*2.5f, rect.y - _wt_gap);
				//gene function
				pg.fill(120);
				pg.text(g.func, rect.x-_margin*2.5f, rect.y+_margin*1.5f- _wt_gap);

				if(!g.g_class.equals("NA")){
					pg.fill(120);
					pg.text(g.g_class, rect.x-_margin*2.5f, rect.y+_margin*3- _wt_gap);

				}
				if(gene !=null){
					//x 
					pg.textFont(small_font);
					pg.textAlign(CENTER, TOP);
					for(int j :conf.x_axis_ticks){
						float dx = map(j, conf.x_min, conf.x_max, 0, conf.box_w) + rect.x;
						pg.fill(180);
						pg.text(j, dx, rect.y+rect.height);
						//tick marks
						pg.stroke(160);
						pg.strokeWeight(1);
						pg.line(dx, rect.y+rect.height, dx, rect.y+rect.height -2);
					}
					//axis label
					pg.fill(180);
					pg.text(conf.x_axis_label, (float)rect.getCenterX(), rect.y+rect.height+_margin);

					//y
					pg.textAlign(RIGHT, CENTER);
					for(int j: conf.y_axis_ticks){
						float dy = map(j, conf.y_min, conf.y_max, conf.box_h, 0) + rect.y;
						pg.fill(180);
						pg.text(j, rect.x-2, dy);
						//tick marks
						pg.stroke(160);
						pg.strokeWeight(1);
						pg.line(rect.x, dy, rect.x+2, dy);

					}
					//wt label
					pg.fill(180);
					pg.text("WT", rect.x-2, rect.y - _margin-2);
					//label
					pg.pushMatrix();
					pg.translate(rect.x-_margin*1.5f, (float)rect.getCenterY());
					pg.rotate(-PI/2);
					pg.textAlign(CENTER, BOTTOM);
					pg.text(conf.y_axis_label, 0, 0);
					pg.popMatrix();
					pg.textFont(large_font);
				}
			}
			if(k != 0 && gene != null){ //skip the first gene
				//celll line
				pg.textFont(small_font);
				pg.textAlign(CENTER, BOTTOM);
				pg.fill(180);

				String c_label = split(c.name, "_")[0];
				// pg.text(c_label, (float)rect.getCenterX(),rect.y - _wt_gap);
				pg.textFont(large_font);
			}
			
		}
		runningX += conf.box_w +_h_gap;
	}
	pg.endDraw();
}



public String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

public String timestamp_detail() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$tm%1$td%1$ty_%1$tH%1$tM", now);
}

public void createLollypop(String gene_name){
	Frame f = new Frame(gene_name);
	Lollypop lp = new Lollypop(this, gene_name, f, conf.hotspot_folder_url+"/"+gene_name+".txt");
	f.add(lp);
	lp.init();
	f.setTitle(gene_name);
	f.setSize(lp.w, lp.h);
	f.setLocation(0,0);
	f.setResizable(false);
	f.setVisible(true);

	f.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent windowEvent){
	    	Frame frame = (Frame)windowEvent.getComponent();
	    	frame.setVisible(false);
	    	frame.dispose();
	    	println("close!");
		}        
	});    
	
}

//create data object for lollypop
public LollypopData getLollypopData(String name){
	LollypopData result = new LollypopData(name);
	//for each cell line
	for(int i = 0; i < all_cells.size(); i++){
		Cell c = all_cells.get(i);	
		//see if the gene exist
		Gene gene = (Gene) c.gene_map.get(name);
		if(gene != null){
			result.addGene(c, gene);
		}
	}
	Collections.sort(result.pops);
	return result;
}

class Cell{
	String name;
	ArrayList<Gene> genes;
	HashMap<String, Gene> gene_map;

	Cell(String name){
		this.name = name;
		this.genes = new ArrayList<Gene>();
		this.gene_map = new HashMap<String, Gene>();
	}

	public String toString(){
		return name+"("+genes.size()+")";
	}

	
}
class Configuration{
	int box_w = _margin*10;
	int box_h = _margin*15;
	String x_axis_label = "signature robustness";
	int[] x_axis_ticks = {0, 100};
	String y_axis_label = "MUT connectivity to WT(MI)";
	int[] y_axis_ticks = {-100, 0, 100};
	int x_min = 0;
	int x_max = 105;
	int y_min = -105;
	int y_max = 105;

	String input_file_url;
	String hotspot_folder_url;
	String ie_file_url;
	String local_url;

	ArrayList<String> cell_lines; 
	ArrayList<Integer> robustness;
	ArrayList<Integer> connectivity;

	String msg = "";
	boolean isError = false;

	//default constructor
	Configuration(){
		input_file_url = "";
		hotspot_folder_url = "";
		ie_file_url = "";
		local_url = "";
		cell_lines = new ArrayList<String>();
		robustness = new ArrayList<Integer>();
		connectivity = new ArrayList<Integer>();
	}

	Configuration(String[] lines, String local){
		input_file_url = "";
		hotspot_folder_url = "";
		ie_file_url = "";
		local_url = local;
		cell_lines = new ArrayList<String>();
		robustness = new ArrayList<Integer>();
		connectivity = new ArrayList<Integer>();
		for(String l: lines){
			if(l.trim().length() > 0){
				String[] s = split(l, "\t");
				if(l.startsWith("#")){
					if(s[0].trim().toLowerCase().equals("#box_w")){
						box_w = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#box_h")){
						box_h = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#x_axis")){
						x_axis_label = s[1].trim();
						ArrayList<Integer> ticks = new ArrayList<Integer>();
						for(int i = 2; i < s.length; i++){
							if(!s[i].trim().equals("")){
								int tick = Integer.parseInt(s[i]);
								ticks.add(tick);
							}
						}
						x_axis_ticks = new int[ticks.size()];
						for(int i = 0; i< ticks.size(); i++){
							x_axis_ticks[i] = ticks.get(i).intValue();
						}
						
					}else if(s[0].trim().toLowerCase().equals("#y_axis")){
						y_axis_label = s[1].trim();
						ArrayList<Integer> ticks = new ArrayList<Integer>();
						for(int i = 2; i < s.length; i++){
							if(!s[i].trim().equals("")){
								int tick = Integer.parseInt(s[i]);
								ticks.add(tick);
							}
						}
						y_axis_ticks = new int[ticks.size()];
						for(int i = 0; i< ticks.size(); i++){
							y_axis_ticks[i] = ticks.get(i).intValue();
						}
					}else if(s[0].trim().toLowerCase().equals("#x_min")){
						x_min = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#x_max")){
						x_max = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#y_min")){
						y_min = Integer.parseInt(s[1]);
					}else if(s[0].trim().toLowerCase().equals("#y_max")){
						y_max = Integer.parseInt(s[1]);
					}
				}else{
					if(s[0].trim().toLowerCase().equals("input")){
						input_file_url = s[1].trim();
						//check if file exist
						File f = new File(this.input_file_url);
						if(f.exists()){
							//exist
							select_input_file(f);
						}else{
							//check in data folder
							f = new File(dataPath(this.input_file_url));
							if(f.exists()){
								//exists
								input_file_url = dataPath(this.input_file_url);
								select_input_file(f);
							}else{
								//check in relative folder
								f = new File(local_url+"/"+input_file_url);
								if(f.exists()){
									//file is in the relative urls
									input_file_url = local_url+"/"+input_file_url;
									println("debug:loading relative path:"+input_file_url);
									select_input_file(f);
								}else{
									println("debug:loading relative path was not found:"+local_url+"/"+input_file_url);
									f = null;
									isError = true;
									if(this.input_file_url.equals("")){
										msg += "The input file url is not defined.\t";
									}else{
										msg += "The specified file ("+this.input_file_url+") was not found. \n";
									}
								}
							}
						}
					}else if(s[0].trim().toLowerCase().equals("hotspot")){
						hotspot_folder_url = s[1].trim();
						//check if file exist
						File f = new File(hotspot_folder_url);
						if(f.exists()){
							//exist

						}else{
							//check in data folder
							f = new File(dataPath(hotspot_folder_url));
							if(f.exists()){
								//exists
								hotspot_folder_url = dataPath(hotspot_folder_url);
							}else{
								if(hotspot_folder_url.equals("")){
									//not defined
								}else{
									f = new File(local_url+"/"+hotspot_folder_url);
									if(f.exists()){
										println("debug: found hotspot folder:"+f.getAbsolutePath());
										hotspot_folder_url = local_url+"/"+hotspot_folder_url;
									}else{
										f = null;
										isError = true;
										msg += "The specified folder ("+hotspot_folder_url+") was not found. \n";
									}
								}
							}
						}
						if(f == null){
							hotspot_file = null;
						}else{
							if(f.isDirectory()){
								select_hotspot_folder(f);
							}else{
								f = null;
								isError = true;
								msg += "The specified url ("+hotspot_folder_url+") is not a directory. \n";
							}
						}
					}else if(s[0].trim().toLowerCase().equals("ie")){
						ie_file_url = s[1].trim();
						//check if file exist
						File f = new File(ie_file_url);
						if(f.exists()){
							//exist
						}else{
							//check in data folder
							f = new File(dataPath(ie_file_url));
							if(f.exists()){
								//exists
							}else{
								if(ie_file_url.equals("")){
									//not defined									
								}else{
									f = new File(local_url+"/"+ie_file_url);
									if(f.exists()){
										ie_file_url = local_url+"/"+ie_file_url;
									}else{
										f = null;
										isError = true;
										msg += "The specified file ("+ie_file_url+") was not found. \n";
									}
								}
							}
						}
						if(f == null){
							ie_file = null;
						}else{
							select_ie_file(f);
						}
					}else if(s[0].trim().toLowerCase().equals("cell")){
						// celll line
						String name = s[1].trim();
						int c_rob = Integer.parseInt(s[2]);
						int c_con = Integer.parseInt(s[3]);

						cell_lines.add(name);
						robustness.add(c_rob);
						connectivity.add(c_con);
					}
				}
			}
		}
		// println("debug: new configuration: \n"+exportConfiguration());
	}

	public String exportConfiguration(){
		String output = "";
		output += "#box_width\t"+box_w+"\n";
		output += "#box_height\t"+box_h+"\n";
		output += "#x_axis\t"+x_axis_label;
		for(int i :x_axis_ticks){
			output+= "\t"+i;
		}
		output += "\n";
		output += "#y_axis\t"+y_axis_label;
		for(int i : y_axis_ticks){
			output += "\t"+i;
		}
		output += "\n";
		output += "#x_min\t"+x_min+"\n";
		output += "#x_max\t"+x_max+"\n";
		output += "#y_min\t"+y_min+"\n";
		output += "#y_max\t"+y_max+"\n";


		output += "input\t"+input_file_url+"\n";
		output += "hotspot\t"+hotspot_folder_url+"\n";
		output += "ie\t"+ie_file_url+"\n";

		for(int i = 0; i< cell_lines.size(); i++){
			output += "cell\t"+cell_lines.get(i)+"\t"+robustness.get(i)+"\t"+connectivity.get(i)+"\n";
		}
		return output;
	}


	public boolean isReady(){
		msg = "";
		isError = false;
		include_hotspot = false;
		include_IE = false;
		//check file existance
		File f = new File(dataPath(input_file_url));
		if(!f.exists()){
			f = new File(input_file_url);
			if(!f.exists()){
				isError = true;
				msg += "the input file was not found. \n";
			}
		}else{
			input_file_url = dataPath(input_file_url);
		}
		if(!hotspot_folder_url.equals("")){
			f = new File(dataPath(hotspot_folder_url));
			if(!f.exists()){
				f = new File(hotspot_folder_url);
				if(!f.exists()){
					isError = true;
					msg += "the hotspot data folder was not found. \n";
				}else{
					include_hotspot = true;
				}
			}else{
				hotspot_folder_url = dataPath(hotspot_folder_url);
				include_hotspot = true;
			}
		}
		if(!ie_file_url.equals("")){
			f = new File(dataPath(ie_file_url));
			if(!f.exists()){
				f = new File(ie_file_url);
				if(!f.exists()){
					isError = true;
					msg += "the ie data file was not found. "+ie_file_url+" \n";
				}else{
					include_IE = true;
				}
			}else{
				ie_file_url = dataPath(ie_file_url);
				include_IE = true;
			}
		}
		return isError?false:true;

	}


	public boolean isValidURL(String filename){
		if (filename.contains(":")) {  // at least smells like URL
			try {
				URL url = new URL(filename);
				InputStream stream = url.openStream();
				return true;
			} catch (MalformedURLException mfue) {
				// not a url, that's fine
				return false;
			} catch (IOException e) {
				// changed for 0117, shouldn't be throwing exception
				e.printStackTrace();
				//System.err.println("Error downloading from URL " + filename);
				return false;
			}
	    }
	    return false;
	}

}

public void keyPressed(){
	if(!isFileSelected){

	}else{
		if(isFileLoaded){
			if(key == 'p'){
				//save pdf
				// println("save pdf");
				// record = true;
				// redraw();
				println("save pdf");
				//save pdf
				String path = timestamp()+".pdf";
				//determine export image size
				PGraphics pdf = createGraphics(stage_width, stage_height, PDF, path);
				pdf.beginDraw();
				pdf.smooth();
				update_display(pdf, 255);
				pdf.dispose();
				pdf.endDraw();
			}else if(key == 'l'){
				println("press l");
				//toggle labels
				show_labels  = !show_labels;
				update_display(p, 255);
				// update_display(p, transparency);
				loop();
			}
		}
	}
}



public void mouseMoved(){
	if(!isFileSelected){
		cursor(ARROW);
		for(TextButton tb: textbtns){
			if(tb.area.contains(mouseX, mouseY)){
				cursor(HAND);
				loop();
				return;
			}
		}
		for(NumberBox nb: rob_numberboxes){
			if(nb.contains(mouseX, mouseY)){
				cursor(HAND);
				loop();
				return;
			}
		}
		for(NumberBox nb: con_numberboxes){
			if(nb.contains(mouseX, mouseY)){
				cursor(HAND);
				loop();
				return;
			}
		}

		loop();
	}else{
		cursor(ARROW);
		hovering_cell = null;
		hovering_gene = null;
		hovering_mutation = null;
		hovering_wt = false;
		hovering_gene_name = null;

		if(scroll_bar.contains(mouseX, mouseY)){
			cursor(HAND);
		}else if(mouseX < _left_margin){
			//check if it is hoving the name
			for(Rectangle r: gene_hit_area){
				if(r.contains(mouseX, mouseY-image_y_pos)){
					cursor(HAND);
					int m_y = mouseY - image_y_pos - _top_margin + _v_gap;
					int range = stage_height - _top_margin - _bottom_margin + _v_gap;
					int index = constrain(floor(map(m_y, abs(_top_margin-_v_gap), range, 0, n_row)), 0, all_genes.size()-1);
					hovering_gene_name = all_genes.get(index);
					return;
				}
			}
		}else{
			//find the current gene index from image_y_pos
			// int m_y = mouseY - image_y_pos - _top_margin;
			// int range = stage_height - _top_margin - _bottom_margin +_v_gap;
			// int index = constrain(floor(map(m_y, 0, range, 0, n_row)), 0, all_genes.size()-1);
			
			int m_y = mouseY - image_y_pos - _top_margin + _v_gap;
			int range = stage_height - _top_margin - _bottom_margin + _v_gap;
			int index = constrain(floor(map(m_y, abs(_top_margin-_v_gap), range, 0, n_row)), 0, all_genes.size()-1);

			int mouse_y = mouseY - image_y_pos;

			Gene gene = all_genes.get(index);
			//check if cursor in the box
			outerloop:
			for(int i = 0; i< gene.rects.length; i++){
				Rectangle r = gene.rects[i];
				if(r.x - _margin < mouseX && mouseX < r.x+r.width+_margin && mouse_y < r.y+r.height){
					//hit
					Cell cell = all_cells.get(i);
					//check if the gene exist
					Gene ge = (Gene) cell.gene_map.get(gene.name);
					if(ge != null){
						// println("debug:"+gene.name+" of "+ cell.name + "  m_y="+m_y+"  mouseY="+mouseY);
						cursor(CROSS);

						float closest = 3f;
						Mutation closest_m = null;
						for(Mutation m : ge.mutations){
							float dist = dist(mouseX, mouse_y, m.dx, m.dy);
							if(dist < closest){
								closest_m = m;
								closest = dist;
							}
						}
						if(closest_m != null){
							hovering_cell = cell;
							hovering_gene = ge;
							hovering_mutation = closest_m;
							cursor(target_cursor);
							println("debug:"+ge.name+" of "+ cell.name +"  "+ closest_m.name);
							break outerloop;
						}else{
							//check hovering wt
							float dist = dist(mouseX, mouse_y, ge.wt_dx, ge.wt_dy);
							if(dist < closest){
								hovering_cell = cell;
								hovering_gene = ge;
								hovering_wt = true;
								cursor(target_cursor);
								break outerloop;
							}
						}
					}
					break outerloop;
				}
			}
		}
		loop();
	}
	
}

public void mousePressed(){
	if(!isFileSelected){
		for(TextButton tb: textbtns){
			if(tb.area.contains(mouseX, mouseY)){
				if(tb.name.equals("input_btn")){
					//select input file
					selectInput("Select an input file:", "select_input_file");
				}else if(tb.name.equals("hotspot_btn")){
					//select hotspot directory
					selectFolder("Select the hotspot file directory:", "select_hotspot_folder");
				}else if(tb.name.equals("ie_btn")){
					//select ie file
					selectInput("Select an Infection Efficiency file:", "select_ie_file");
				}else if(tb.name.equals("conf_load_btn")){
					//load configuration
					selectInput("Select a configuration file:", "select_conf_load");

				}else if(tb.name.equals("conf_save_btn")){
					selectFolder("Select a folder to save the configuration file:", "select_conf_save");
				}else if(tb.name.equals("load_btn")){
					//check if the configuration is ready to load
					if(conf.isReady()){
						//start loading
						println("debug: it is ready to load!");
						start_loading();
					}else{
						//pop up message
						println("debug: it is NOT ready to load!\n"+conf.msg);
						try {
							//pop up
							javax.swing.JOptionPane.showMessageDialog(null, "Some errors with the current configuration:\n"+conf.msg);
						} catch (Exception e) {
						   println("error on load_btn click:"+e);
						}

					}
				}
			}
		}
		//number box
		for(NumberBox nb: rob_numberboxes){
			if(nb.contains(mouseX, mouseY)){
				nb.evaluate(mouseX, mouseY, 1.0f);
				loop();
				return;
			}
		}
		for(NumberBox nb: con_numberboxes){
			if(nb.contains(mouseX, mouseY)){
				nb.evaluate(mouseX, mouseY, 1.0f);
				loop();
				return;
			}
		}		
	}else{
		if(scroll_bar.contains(mouseX, mouseY)){
			isScrolling = true;
			m_pressed_y = mouseY;
		}

		if(hovering_gene_name != null){
			if(include_hotspot){
				createLollypop(hovering_gene_name.name);
			}
		}
	}
}

public void mouseDragged(){
	if(!isFileSelected){
	}else{
		if(isScrolling){
			int diff = mouseY  - m_pressed_y; //how many pixel it moved
			int prev_bar_y = scroll_bar.y;
			int new_bar_y = constrain(prev_bar_y + diff, scroll_area.y, scroll_area.y+scroll_area.height-scroll_bar.height);
			scroll_bar.setLocation(scroll_bar.x, new_bar_y);
			m_pressed_y = mouseY;
			//update image position
			image_y_pos = -round(map(new_bar_y, scroll_area.y, scroll_area.y+scroll_area.height, 0, stage_height));
			
			loop();
		}
	}
}

public void mouseReleased(){
	if(!isFileSelected){
	}else{
		isScrolling = false;
		hovering_gene_name = null;
		loop();
	}
}
String app_title = "Sparkler Plot";
int app_title_x, app_title_y;
PFont title_font;

//box parameters
int btn_height = _margin*2;
int btn_width = _margin*10;
int nb_width = _margin*5;
int fileName_width = _margin*30;
int btn_gap = _margin*2;

ArrayList<TextBox> textboxes;
ArrayList<TextButton> textbtns;
ArrayList<String> texts;
ArrayList<PVector> text_positions;

// ArrayList<NumberBox> numberboxes = null;

ArrayList<NumberBox> con_numberboxes, rob_numberboxes;

//cell line
// ArrayList<String> unique_cell_line = null;


//hovering
int color_active = c_blue;

//File
File input_file = null;
File hotspot_file = null;
File ie_file = null;

//TextBox
TextBox input_box, hotspot, ie, conf_load, conf_save;
TextButton hotspot_btn, ie_btn, conf_load_btn, conf_save_btn, load_btn;


//Status
boolean isError = false;
String error_msg = "";

//Optional data
boolean isHotspotData = false;
boolean isIEData = false;


public void setup_data_loading_page(){
	textboxes = new ArrayList<TextBox>();
	textbtns = new ArrayList<TextButton>();
	texts = new ArrayList<String>();
	text_positions = new ArrayList<PVector>();
	// numberboxes = new ArrayList<NumberBox>();
	rob_numberboxes = new ArrayList<NumberBox>();
	con_numberboxes = new ArrayList<NumberBox>();
	

	int runningX = _left_margin;
	int runningY = _top_margin;
	//title
	app_title_x = runningX;
	app_title_y = runningY;
	title_font = createFont("SansSerif", 30);
	runningY += _margin * 8;

	//input file
	input_box = new TextBox("input", runningX, runningY, fileName_width, btn_height);
	input_box.instruction = "Please select an input file:";
	textboxes.add(input_box);
	//input btn
	TextButton input_btn = new TextButton("input_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	input_btn.instruction = "select";
	textbtns.add(input_btn);
	runningY += btn_height + btn_gap + _margin*2;


	//optional files
	//hotspot
	hotspot = new TextBox("hotspot", runningX, runningY, fileName_width, btn_height);
	hotspot.instruction ="Optional: Select a hotspot file directory:";
	textboxes.add(hotspot);
	hotspot_btn = new TextButton("hotspot_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	hotspot_btn.instruction = "select";
	textbtns.add(hotspot_btn);
	runningY += btn_height + btn_gap;

	//ie
	ie = new TextBox("ie", runningX, runningY, fileName_width, btn_height);
	ie.instruction ="Optional: Select a infection efficiency file:";
	textboxes.add(ie);
	ie_btn = new TextButton("ie_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	ie_btn.instruction = "select";
	textbtns.add(ie_btn);
	runningY += btn_height + btn_gap;

	//load configuration file
	conf_load = new TextBox("conf_load", runningX, runningY, fileName_width, btn_height);
	conf_load.instruction ="Optional: Load a configuration file:";
	textboxes.add(conf_load);
	conf_load_btn = new TextButton("conf_load_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	conf_load_btn.instruction = "select";
	textbtns.add(conf_load_btn);
	runningY += btn_height + btn_gap;

	//save configuration
	conf_save = new TextBox("conf_save", runningX, runningY, fileName_width, btn_height);
	conf_save.instruction ="Optional: Save a configuration file:";
	conf_save.text = "conf_"+timestamp_detail()+".txt";
	textboxes.add(conf_save);
	conf_save_btn = new TextButton("conf_save_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	conf_save_btn.instruction = "save";
	textbtns.add(conf_save_btn);
	runningY += btn_height + btn_gap;

	//load btn
	load_btn = new TextButton("load_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	load_btn.instruction ="load";
	textbtns.add(load_btn);
	runningY += btn_height +btn_gap;

}

public void update_data_loading_page(){
	println("debug ---- start of update_data_loading_page() ----");
	int runningX = _left_margin;
	int runningY = _top_margin;
	texts = new ArrayList<String>();
	text_positions = new ArrayList<PVector>();
	//title
	app_title_x = runningX;
	app_title_y = runningY;
	title_font = createFont("SansSerif", 30);
	runningY += _margin * 8;

	//input file
	//input btn
	runningY += btn_height + btn_gap + _margin*2;

	//cell line
	texts.add("cell line");
	text_positions.add(new PVector(runningX, runningY));
	texts.add("thresholds");
	text_positions.add(new PVector(runningX + btn_width +_margin*4, runningY));
	runningY += btn_height +_margin;

	//cell line number boxes
	// numberboxes = new ArrayList<NumberBox>();
	rob_numberboxes = new ArrayList<NumberBox>();
	con_numberboxes = new ArrayList<NumberBox>();
	con_numberboxes = new ArrayList<NumberBox>();


	for(int i = 0; i< conf.cell_lines.size(); i++){
		String cell = conf.cell_lines.get(i);
		// println("debug:cell line:"+cell+"  pos:"+runningX+"  "+runningY);
		texts.add(cell);
		text_positions.add(new PVector(runningX, runningY));
		//robustness
		NumberBox robustness = new NumberBox(runningX+btn_width+_margin*4, runningY, nb_width, btn_height, conf.robustness.get(i), 100, 0, cell+"_rob");
		robustness.instruction = "robustness";
		rob_numberboxes.add(robustness);

		//connectivity
		NumberBox connectivity = new NumberBox(runningX+btn_width+nb_width+btn_gap+_margin*5, runningY, nb_width, btn_height, conf.connectivity.get(i), 100, 0, cell+"_con");
		connectivity.instruction = "connectivity";
		con_numberboxes.add(connectivity);
		
		runningY += btn_height + btn_gap;
	}

	//position optional files options
	runningY += _margin*2;
	hotspot.area = new Rectangle(runningX, runningY, fileName_width, btn_height);
	hotspot_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height + btn_gap;

	ie.area = new Rectangle(runningX, runningY, fileName_width, btn_height);
	ie_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height + btn_gap;

	conf_load.area = new Rectangle(runningX, runningY, fileName_width, btn_height);
	conf_load_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height + btn_gap;

	conf_save.area = new Rectangle(runningX, runningY, fileName_width, btn_height);
	conf_save_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height + btn_gap;

	//load btn
	load_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height +btn_gap;

	println("debug ---- end of update_data_loading_page() ----");
}

public TextBox getTextBox(String name){
	for(TextBox tb: textboxes){
		if(tb.name.equals(name)){
			return tb;
		}
	}
	return null;
}

//while file is being selected
public void draw_data_loading_page(){

	//draw title
	textFont(title_font);
	fill(120);
	textAlign(LEFT, TOP);
	text(app_title, app_title_x, app_title_y);

	//input
	textFont(font);
	//text boxes
	for(TextBox tb: textboxes){
		tb.render();
	}
	//text btns
	for(TextButton tb: textbtns){
		tb.render();
	}

	if(rob_numberboxes != null){
		for(NumberBox nb: rob_numberboxes){
			nb.render();
		}
	}
	if(con_numberboxes != null){
		for(NumberBox nb: con_numberboxes){
			nb.render();
		}
	}

	//text labels
	fill(120);
	textAlign(LEFT, TOP);
	for(int i = 0; i < texts.size(); i++){
		PVector pos = text_positions.get(i);
		text(texts.get(i),pos.x, pos.y);
	}		
}


//input file is selected
public void select_input_file(File selection){
	if(selection == null){
		input_file = null;
		input_box.text = "";
	}else{
		input_file = selection;
		String url = selection.getAbsolutePath();
		conf.input_file_url = url;
		if(url.length() > 40){
			input_box.text = "..."+url.substring((url.length()-37), (url.length()));
		}else{
			input_box.text = url;
		}

		// pre parse the file
		// find the number of unique cell lines
		String[] lines = loadStrings(selection);

		for(int i = 0; i < lines.length; i++){
			String l = lines[i];
			String[] s = split(l, "\t");
			if(i==0){
				//header
				if(s.length < 7){
					isError = true;
					error_msg = "Please check your input file structure.";
				}else if(!s[0].toLowerCase().startsWith("gene")){
					isError = true;
					error_msg = "Please check your input file structure.";
				}else if(!s[6].toLowerCase().startsWith("cell_line")){
					isError = true;
					error_msg = "Please check your input file structure.";
				}

				if(isError){
					break;
				}
			}else{
				String cell_line = s[6].trim();
				if(!conf.cell_lines.contains(cell_line)){
					conf.cell_lines.add(cell_line);
				}
			}
		}
	}

	if(conf.cell_lines.size() != conf.robustness.size()){
		conf.robustness = new ArrayList<Integer>();
		conf.connectivity = new ArrayList<Integer>();
		//set default
		for(String c:conf.cell_lines){
			conf.robustness.add(73);
			conf.connectivity.add(90);
		}
	}

	// println("MI file is scanned: "+conf.cell_lines);
	update_data_loading_page();
	loop();
}

public void download_input_file(String filename){
	try {
		URL url = new URL(filename);
		InputStream stream = url.openStream();

		if(filename.length() > 40){
			input_box.text = "..."+filename.substring((filename.length()-37), (filename.length()));
		}else{
			input_box.text = filename;
		}

		// pre parse the file
		// find the number of unique cell lines
		String[] lines = loadStrings(stream);
		println("debug:"+Arrays.toString(lines));

		for(int i = 0; i < lines.length; i++){
			String l = lines[i];
			String[] s = split(l, "\t");
			if(i==0){
				//header
				if(s.length < 7){
					isError = true;
					error_msg = "Please check your input file structure.";
				}else if(!s[0].toLowerCase().startsWith("gene")){
					isError = true;
					error_msg = "Please check your input file structure.";
				}else if(!s[6].toLowerCase().startsWith("cell_line")){
					isError = true;
					error_msg = "Please check your input file structure.";
				}

				if(isError){
					break;
				}
			}else{
				String cell_line = s[6].trim();
				if(!conf.cell_lines.contains(cell_line)){
					conf.cell_lines.add(cell_line);
				}
			}
		}
		if(conf.cell_lines.size() != conf.robustness.size()){
			conf.robustness = new ArrayList<Integer>();
			conf.connectivity = new ArrayList<Integer>();
			//set default
			for(String c:conf.cell_lines){
				conf.robustness.add(73);
				conf.connectivity.add(90);
			}
		}

		println("MI file is scanned: "+conf.cell_lines);
		update_data_loading_page();
		loop();
	} catch (MalformedURLException mfue) {
		// not a url, that's fine
	} catch (IOException e) {
		// changed for 0117, shouldn't be throwing exception
		e.printStackTrace();
		//System.err.println("Error downloading from URL " + filename);
	}
}


//hotspot folder is selected
public void select_hotspot_folder(File selection){
	println("select_hotpost_folder():"+selection.getAbsolutePath());
	if(selection == null){
		hotspot_file = null;
		isHotspotData = false;
	}else{
		//check if it is a directory
		if(selection.isDirectory()){
			hotspot_file = selection;
			isHotspotData = true;
			String url = selection.getAbsolutePath();
			conf.hotspot_folder_url = url;
			if(url.length() > 40){
				hotspot.text = "..."+url.substring((url.length()-37), (url.length()));
			}else{
				hotspot.text = url;
			}
		}else{
			try {
				//pop up
				javax.swing.JOptionPane.showMessageDialog(null, "Please select a directory.");
			} catch (Exception e) {
			   println("error on load_btn click:"+e);
			}
		}
	}
	loop();
}


public void select_ie_file(File selection){
	if(selection == null){
		ie_file = null;
		isIEData = false;
	}else{
		ie_file = selection;
		isIEData = true;
		String url = selection.getAbsolutePath();
		conf.ie_file_url = url;
		if(url.length() > 40){
			ie.text = "..."+url.substring((url.length()-37), (url.length()));
		}else{
			ie.text = url;
		}
	}
	loop();
}


public void select_conf_load(File selection){
	if(selection == null){
	}else{
		String url = selection.getAbsolutePath();
		conf.local_url = selection.getParent();
		// println("debug: file is selected:"+conf.local_url);

		if(url.length() > 40){
			conf_load.text = "..."+url.substring((url.length()-37), (url.length()));
		}else{
			conf_load.text = url;
		}

		//parse configuration
		// String msg = "";

		String[] lines = loadStrings(selection);
		Configuration new_conf = new Configuration(lines, conf.local_url);
		if(new_conf.isError){
			try {
				//pop up
				javax.swing.JOptionPane.showMessageDialog(null, "Error with loading the configuration file:\n"+new_conf.msg);
			} catch (Exception e) {
			   println("error on load_btn click:"+e);
			}
		}else{
			conf = new_conf;
			//update
			update_data_loading_page();
		}
	}


	loop();
}

public void select_conf_save(File selection){
	if(selection == null){
	}else{
		if(selection.isDirectory()){
			String url = selection.getAbsolutePath()+"/"+conf_save.text;
			//get configuration output
			String output = conf.exportConfiguration();
			// println("output ::::: \n"+output);
			//save a text file

			PrintWriter writer = createWriter(url);
			writer.println(output);
			writer.flush();
			writer.close();
			// println("debug: save url:"+url);
			try {
				//pop up
				javax.swing.JOptionPane.showMessageDialog(null, conf_save.text+" is saved.");
			} catch (Exception e) {
			   println("error on configuration save click:"+e);
			}


		}else{
			try {
				//pop up
				javax.swing.JOptionPane.showMessageDialog(null, "Please select a directory.");
			} catch (Exception e) {
			   println("error on load_btn click:"+e);
			}
		}
	}
}

public void load_data(String url){	
	String[] lines = loadStrings(url);
	for(String l :lines){
		if(l.startsWith("gene")){
			//header
		}else{
			String[] s = split(l, "\t");
			String gene_name = s[0].trim();
			String mut_name = s[1].trim();
			float mut_rep = Float.parseFloat(s[2]);
			float wt_rep = Float.parseFloat(s[3]);
			float mut_wt_con = Float.parseFloat(s[4]);
			String wt_name = s[5].trim();
			String cell_line = s[6].trim();
			String g_func = s[7].trim();
			String g_class = s[8].trim();

			// if(mut_name.contains("_WT")){
			// 	println("debug:ignore wild type:"+mut_name);
			// }else{
				//find cell
				Cell cell = (Cell)cell_map.get(cell_line);
				if(cell == null){
					cell = new Cell(cell_line);
					cell_map.put(cell_line, cell);
					all_cells.add(cell);
				}

				//find Gene for sorting
				Gene gene = (Gene)gene_map.get(gene_name);
				if(gene == null){
					gene = new Gene(gene_name, g_func, g_class);
					gene_map.put(gene_name, gene);
					all_genes.add(gene);

					if(!gene_functions.contains(g_func)){
						gene_functions.add(g_func);
					}
				}

				//within the cell line
				Gene g = (Gene)cell.gene_map.get(gene_name);
				if(g == null){
					g = new Gene(gene_name, wt_name, wt_rep);
					cell.gene_map.put(gene_name, g);
					cell.genes.add(g);
				}
				//add mutation
				Mutation mu = new Mutation(mut_name, mut_rep, mut_wt_con);
				g.mutations.add(mu);
			// }
		}
	}
}


//load IE information
public void load_ie_list(String url){

	String[] lines = loadStrings(url);
	int[] indexes = new int[all_cells.size()];
	Arrays.fill(indexes, -1);
	for(String l : lines){
		if(l.startsWith("gene")){
			//header
			//find index of cell 
			String[] s = split(l, "\t");
			for(int i = 2; i < s.length; i++){
				String cell_line_name = s[i].trim();
				for(int j = 0; j < all_cells.size(); j++){
					if(cell_line_name.equals(all_cells.get(j).name)){
						indexes[j] = i;
					}
				}
			}
			println("debug: Array Index:"+Arrays.toString(indexes));
		}else{
			String[] s= split(l, "\t");
			String gene_name = s[0].trim();
			String mut_name = s[1].trim();
			
			//check if it is wild type
			boolean is_wt = false;
			String[] mut_name_array = split(mut_name, "_");
			if(mut_name_array.length >= 2){
				if(mut_name_array[1].startsWith("WT")){
					is_wt = true;
				}
			}	

			for(int i = 0; i < all_cells.size(); i++){
				if(indexes[i] != -1){
					Cell cell = all_cells.get(i);
					//get Gene
					Gene gene = (Gene) cell.gene_map.get(gene_name);
					if(gene != null){
						if(is_wt){
							if(gene.wt_ie == -1f){
								//store the wt ie
								float v = Float.parseFloat(s[indexes[i]]);
								gene.wt_ie = v;
							}
						}else{
							//find mutation
							Mutation mut = gene.findMutation(mut_name);
							if(mut != null){
								float v = Float.parseFloat(s[indexes[i]]);
								mut.ie = v;
								// println("debug: cell:"+cell.name+"\tgene:"+gene_name+"\tmut:"+mut_name+"\tv:"+v);
							}
						}
					}	
				}
			}
		}
	}
}

//load hotspot information
public void load_hotspot(String url){
	//parse per gene
	for(Gene g: all_genes){
		String g_name = g.name;
		//check if file exist in the folder
		File f = new File(url+"/"+g_name+".txt");



		if(f.exists()){
			String[] lines = loadStrings(f);
			int last_pos = Integer.parseInt(split(lines[1], "\t")[1]);
			int total_count = Integer.parseInt(split(lines[2], "\t")[1]);
			//make histogram
			int[] hist = new int[last_pos+1];
			for(int i = 4; i < lines.length; i++){
				String[] s = split(lines[i], "\t");
				int pos = Integer.parseInt(s[0]);
				int count = Integer.parseInt(s[1]);
				hist[pos] = count;
			}

			//for each cell line
			for(Cell c:all_cells){
				//find Gene
				Gene gene = (Gene)c.gene_map.get(g_name);
				if(gene != null){
					//for each mutation, calculate score and determine if it is a hotspot
					for(Mutation m: gene.mutations){
						int pos = m.pos;
						//some mutations positions are not parsed
						if(pos != -1){
							int zero = hist[pos];
							int minus_2 = ((pos-2)>0)?hist[pos-2]:0;
							int minus_1 = ((pos-1)>0)?hist[pos-1]:0;
							int plus_1 = ((pos+1)<=last_pos)?hist[pos+1]:0;
							int plus_2 = ((pos+2)<=last_pos)?hist[pos+2]:0;

							//find max
							int max_count = 0;
							max_count = max(max_count, minus_2 + minus_1 + zero);
							max_count = max(max_count, minus_1 + zero + plus_1);
							max_count = max(max_count, zero + plus_1 +plus_2);
							float max_ratio = (float)max_count/(float)total_count;

							if(pos > 2){
								if(max_ratio >= 0.02f){
									m.is_hotspot = true;
								}
							}
						}
					}
				}
			}
		}else{
			println("debug **** no hotspot data for: "+g_name+"\t"+url+"/"+g_name+".txt");
		}
	}
}

//parse mutations
//assigny mutation type
//check the IE Difference
public void parse_mutations(){
	for(int i = 0; i < all_cells.size(); i++){
		Cell c = all_cells.get(i);
		x_threshold = conf.robustness.get(i);
		y_threshold = conf.connectivity.get(i);
		
		// println("debug:"+i+" "+c.name);
		for(Gene g:c.genes){
			boolean is_wt_robust = false;
			if(g.wt_rep > x_threshold){
				is_wt_robust = true;
			}
			for(Mutation m:g.mutations){
				boolean is_mu_robust = false;
				boolean is_con = false;
				if(m.rep  > x_threshold){
					is_mu_robust = true;
				}
				if(m.con > y_threshold){
					is_con = true;
				}
				//{"GOF", "LOF", "SI", "NI"};
				if(is_wt_robust){
					if(is_mu_robust){
						if(is_con){
							m.type_index = 2; //silent
						}else{
							m.type_index = 0;
						}
					}else{
						if(is_con){
							m.type_index = 1;
						}else{
							m.type_index = 1;
						}
					}
				}else{
					if(is_mu_robust){
						if(is_con){
							m.type_index = 0;
						}else{
							m.type_index = 0;
						}
					}else{
						if(is_con){
							m.type_index = 3;
						}else{
							m.type_index = 3;
						}
					}
				}
			}
		}
	}	
	//check IE
	for(int i = 1; i < conf.cell_lines.size(); i++){
		String cell_line = conf.cell_lines.get(i);
		Cell cell = (Cell) cell_map.get(cell_line);
		if(cell != null){
			for(Gene gene:cell.genes){
				if(gene.wt_ie == -1f){
					//no wt data
					println("debug:"+gene.name+" has no wt ID data.");
				}else{
					float wt_ie = gene.wt_ie;
					//compare against all mutation
					for(Mutation mut: gene.mutations){
						float mut_ie = mut.ie;
						if(mut_ie == -1f){
							// println("debug:"+cell_line+ " "+ mut.name+" is missing IE data");
						}else{
							// println("debug:"+cell_line+ " "+ mut.name+" is matched IE data");
							float ratio = 0;
							if(wt_ie > mut_ie){
								ratio = wt_ie/mut_ie;
							}else{
								ratio = mut_ie/wt_ie;
							}
							if(ratio > 2){
								//flag
								mut.flag_ie = true;
								// println("flag mutation: "+mut+"  ratio="+ratio);
							}
						}
					}
				}
			}
		}
	}
}
class Gene{
	float wt_rep = 0;
	float wt_con = 100;
	String func = "UNKN";
	String wt_name = "NA";
	String name = "NA";
	String g_class ="NA";


	float wt_ie = -1f;

	ArrayList<Mutation> mutations;
	Rectangle rect;
	Rectangle[] rects;

	float wt_dx, wt_dy; //display position

	Gene(String name, String wt_name, float wt_rep){
		this.name = name;
		this.wt_name = wt_name;
		this.wt_rep = wt_rep;
		this.mutations = new ArrayList<Mutation>();
	}

	//for sorting
	Gene(String name, String func){
		this.name = name;
		this.func = func;
	}
	Gene(String name, String func, String c){
		this.name = name;
		this.func = func;
		this.g_class = c;
	}

	

	public void renderBackground(PGraphics pg, Rectangle r, int index){
		x_threshold = conf.robustness.get(index);
		y_threshold = conf.connectivity.get(index);

		//background
		pg.noStroke();
		pg.fill(255);
		pg.rect(r.x, r.y, r.width, r.height);
		//threshold values
		pg.stroke(240);
		pg.strokeCap(SQUARE);
		float tx = map(x_threshold, conf.x_min, conf.x_max, 0, conf.box_w) + r.x;
		float ty = map(y_threshold, conf.y_min, conf.y_max, conf.box_h, 0) + r.y;
		pg.strokeWeight(1);
		pg.line(tx, r.y, tx, r.y+r.height);
		pg.line(r.x, ty, r.x+r.width, ty);

		//x
		pg.textFont(small_font);
		pg.textAlign(CENTER, TOP);
		pg.fill(180);
		pg.text(round(x_threshold),tx, r.y+r.height);
		//y
		pg.textAlign(RIGHT, CENTER);
		pg.text(round(y_threshold),r.x -2, ty);
		pg.textFont(large_font);
		
		//outline
		pg.noFill();
		pg.stroke(160);
		pg.strokeWeight(1);
		pg.rect(r.x, r.y, r.width, r.height);

		//draw wt line
		
		float wt_y = r.y - _wt_gap;//map(100, conf.y_min, conf.y_max, conf.box_h, 0) + r.y;
		//wt line
		pg.stroke(160);
		pg.strokeWeight(1);
		pg.line(r.x, wt_y, r.x+r.width, wt_y);
		//tick marks
		pg.line(r.x, wt_y-2, r.x, wt_y+2);
		pg.line(r.x + r.width, wt_y-2, r.x+r.width, wt_y+2);
		pg.line(tx, wt_y-2, tx, wt_y+2);
	}


	//PGraphcis
	public void renderPlot(PGraphics pg, Rectangle r, int opacity){
		//draw plot
		float wt_x = map(wt_rep, conf.x_min, conf.x_max, 0, conf.box_w) + r.x;
		float wt_y = r.y - _wt_gap;

		if(wt_x < r.x){
			wt_x = r.x - 2.5f;
		}
		this.wt_dx = wt_x;
		this.wt_dy = wt_y;

		//middle line
		pg.strokeWeight(1);
		pg.stroke(180);
		pg.line(r.x, (float)r.getCenterY(), r.x+r.width, (float)r.getCenterY());


		//draw mutations
		for(Mutation m: mutations){
			float m_x = map(m.rep, conf.x_min, conf.x_max, 0, conf.box_w) + r.x;
			float m_y = map(m.con, conf.y_min, conf.y_max, conf.box_h, 0) + r.y;
			if(m_x < r.x){
				m_x = r.x - 2.5f;
			}

			pg.stroke(colors[m.type_index], opacity);
			pg.strokeWeight(1);
			if(!m.flag_ie){
				pg.line(wt_x, wt_y, m_x, m_y);
			}else{
				dashline(pg, wt_x, wt_y, m_x, m_y, dash_line);
			}
			pg.fill(colors[m.type_index], opacity);
			pg.noStroke();
			pg.ellipse(m_x, m_y, 3, 3);

			//hot spot
			if(m.is_hotspot){
				pg.strokeWeight(0.5f);
				pg.stroke(colors[m.type_index], opacity);
				pg.noFill();
				pg.ellipse(m_x, m_y, 5, 5);
			}


			//mutation label
			if(show_labels){
				pg.textFont(label_font);
				pg.textAlign(RIGHT, TOP);
				pg.fill(0, opacity);
				// pg.fill(colors[m.type_index]);
				String label = "???";
				if(m.name.contains("_")){
					label = split(m.name, "_")[1];
					// if(m.name.contains("WT")){
					// 	label = m.name;
					// }else{
					// }
				}else{
					label = m.name;
				}
				pg.text(label, m_x, m_y);
				pg.textFont(large_font);

				// println("debug: mutation name ="+m.name);
			}

			m.dx = m_x;
			m.dy = m_y;
		}


		//wt dot
		pg.fill(60);
		pg.noStroke();
		pg.ellipse(wt_x, wt_y, 3, 3);
		// pg.stroke(60);
		// pg.strokeWeight(2);
		// pg.strokeCap(ROUND);
		// pg.point(wt_x, wt_y);

		//outline
		if(wt_rep > x_threshold){
			pg.strokeWeight(2);
			pg.stroke(60);
			pg.noFill();
			pg.ellipse(wt_x, wt_y, 8, 8);
		}
	}

	

	public Mutation findMutation(String name){
		for(Mutation m:mutations){
			if(m.name.equals(name)){
				return m;
			}
		}
		// println("debug: mismatch:"+name+":"+mutations);
		return null;
	}

	public String toString(){
		return name;
	}


	//flag mutation 
	public void flag_position(int pos){
		for(Mutation m: mutations){
			if(m.pos == pos){
				m.is_hotspot = true;
				// println("debug: hotspot:"+ this.name+"("+pos+") > "+m.name);
			}
		}
	}
}



public void dashline(PGraphics p, float x0, float y0, float x1, float y1, float[] spacing) { 
  float distance = dist(x0, y0, x1, y1); 
  float [ ] xSpacing = new float[spacing.length]; 
  float [ ] ySpacing = new float[spacing.length]; 
  float drawn = 0.0f;  // amount of distance drawn 
 
  if (distance > 0){ 
    int i; 
    boolean drawLine = true; // alternate between dashes and gaps 
 
    /* 
      Figure out x and y distances for each of the spacing values 
      I decided to trade memory for time; I'd rather allocate 
      a few dozen bytes than have to do a calculation every time 
      I draw. 
    */ 
    for (i = 0; i < spacing.length; i++) { 
      xSpacing[i] = lerp(0, (x1 - x0), spacing[i] / distance); 
      ySpacing[i] = lerp(0, (y1 - y0), spacing[i] / distance); 
    } 
 
    i = 0; 
    while (drawn < distance){ 
      if (drawLine){ 
       p.line(x0, y0, x0 + xSpacing[i], y0 + ySpacing[i]); 
      } 
      x0 += xSpacing[i]; 
      y0 += ySpacing[i]; 
      /* Add distance "drawn" by this line or gap */ 
      drawn = drawn + mag(xSpacing[i], ySpacing[i]); 
      i = (i + 1) % spacing.length;  // cycle through array 
      drawLine = !drawLine;  // switch between dash and gap 
    } 
  } 
} 

public void dashline(float x0, float y0, float x1, float y1, float[] spacing) { 
  float distance = dist(x0, y0, x1, y1); 
  float [ ] xSpacing = new float[spacing.length]; 
  float [ ] ySpacing = new float[spacing.length]; 
  float drawn = 0.0f;  // amount of distance drawn 
 
  if (distance > 0){ 
    int i; 
    boolean drawLine = true; // alternate between dashes and gaps 
 
    /* 
      Figure out x and y distances for each of the spacing values 
      I decided to trade memory for time; I'd rather allocate 
      a few dozen bytes than have to do a calculation every time 
      I draw. 
    */ 
    for (i = 0; i < spacing.length; i++) { 
      xSpacing[i] = lerp(0, (x1 - x0), spacing[i] / distance); 
      ySpacing[i] = lerp(0, (y1 - y0), spacing[i] / distance); 
    } 
 
    i = 0; 
    while (drawn < distance){ 
      if (drawLine){ 
        line(x0, y0, x0 + xSpacing[i], y0 + ySpacing[i]); 
      } 
      x0 += xSpacing[i]; 
      y0 += ySpacing[i]; 
      /* Add distance "drawn" by this line or gap */ 
      drawn = drawn + mag(xSpacing[i], ySpacing[i]); 
      i = (i + 1) % spacing.length;  // cycle through array 
      drawLine = !drawLine;  // switch between dash and gap 
    } 
  } 
}
class GeneComparator implements Comparator<Gene>{
	String type = "name";

	GeneComparator(String type){
		this.type = type;
	}

	public int compare(Gene gene_1, Gene gene_2){

		if(type.equals("name")){
			int result = gene_1.func.compareTo(gene_2.func);
			if(result != 0){
				return result;
			}else{
				return gene_1.name.compareTo(gene_2.name);
			}
		}else if(type.equals("class")){
			int result = gene_1.g_class.compareTo(gene_2.g_class);
			if(result != 0){
				return result;
			}else{
				return gene_1.name.compareTo(gene_2.name);
			}
		}
		// 
			// if(ind_1.colour > ind_2.colour){
			// 	return 1;
			// }else if( ind_1.colour < ind_2.colour){
			// 	return -1;
			// }else{
			// 	return ind_1.name.compareTo(ind_2.name);
			// }
		
		return 0;
	}
}
public class Lollypop extends PApplet {
	String gene_name;
	PApplet parent;
	Frame f;
	int w, h;
	int l_margin = _margin*3;
	int r_margin = _margin*3;
	int hist_width = _margin*10;
	int lolly_width = _margin*10;
	int lolly_size = 8;

	int[] histogram;
	int last_pos;
	int max_count;

	Rectangle h_rect, l_rect;
	LollypopData data;

	PGraphics opaque, transparent;


	Pop selected_pop = null;

	int p_draw_counter = 0;
	int p_draw_max = 30;

	private Lollypop(){}
	private Lollypop(PApplet theParent, String gene_name, Frame f, String url){
		this.parent = theParent;
		this.gene_name = gene_name;
		this.f = f;
		//load data
		String[] lines = loadStrings(url);
		for(String l: lines){
			if(l.startsWith("name")){
				//name
			}else if(l.startsWith("last_pos")){
				//last position
				String[] s = split(l, "\t");
				last_pos = Integer.parseInt(s[1]);
				histogram= new int[last_pos];
			}else if(l.startsWith("max")){
				String[] s = split(l, "\t");
				max_count = Integer.parseInt(s[1]);
			}else{
				String[] s = split(l, "\t");
				int index = Integer.parseInt(s[0]);
				int count = Integer.parseInt(s[1]);
				if(index > 0){
					histogram[index-1] = count;
				}
			}
		}
		//stage size
		int plot_height = round(pos_weight*last_pos);
		w = l_margin + r_margin + hist_width + lolly_width;
		h =  plot_height + _top_margin +_bottom_margin;


		h_rect = new Rectangle(l_margin, _top_margin, hist_width, plot_height);
		l_rect = new Rectangle(l_margin+hist_width, _top_margin, lolly_width, plot_height);

		//create LollypopData
		data = getLollypopData(gene_name);

		//assign positions to lollypop
		data.setPositions(l_rect, last_pos, lolly_size);
		// println("debug:"+data.toString());

		//check the height
		int last_y = data.pops.get(data.pops.size()-1).rect.y;
		if(last_y > h){
			h = last_y + _bottom_margin;
		}
		
	}

	public void setup(){
		size(w, h);

		//prepare PGraphics
		opaque = createGraphics(w, h);
		transparent = createGraphics(w, h);
		update_view(opaque, 255);
		update_view(transparent, 20);
		noLoop();
	}

	public void update_view (PGraphics pg, int opacity){
		pg.beginDraw();
		pg.smooth();
		pg.background(255);
		pg.textFont(small_font);
		int h_axis_x = h_rect.x+h_rect.width;

		//draw histogram
		pg.stroke(120);
		pg.strokeWeight(pos_weight);
		pg.fill(120);
		pg.textAlign(RIGHT, CENTER);
		for(int i = 0; i < last_pos; i++){
			int count = histogram[i];
			if(count > 0){
				int pos = i+1;
				float py = map(pos, 1, last_pos, h_rect.y, h_rect.y+h_rect.height);
				float plength = constrain(map(count, 0, 100, 0, h_rect.width), 0, h_rect.width);
				pg.line(h_axis_x, py, h_axis_x-plength, py);
				if(count >100){
					pg.text(count, h_axis_x-plength-2, py);
				}
			}
		}
		//lollypop
		if(data != null){
			for(Pop p: data.pops){
				//stick
				pg.stroke(0, opacity);
				pg.strokeWeight(1);
				pg.noFill();
				pg.beginShape();
				pg.vertex(p.sx, p.sy);
				pg.vertex(p.sx2, p.sy2);
				pg.vertex(p.sx3, p.sy3);
				pg.endShape();
				// line(l_rect.x, p.dy, l_rect.x+lolly_stick_length, p.dy);
				// ellipse(p.dx, p.dy, lolly_size, lolly_size);
				int num = p.mutations.size();
				float stripe_w = (float)lolly_size / (float)num;
				float left_x = (float)p.bx ;
				float runningX = left_x;
				for(Mutation m: p.mutations){
					pg.fill(colors[m.type_index], opacity);
					pg.noStroke();
					pg.rect(runningX, p.by, stripe_w, lolly_size);
					runningX += stripe_w; 
				}
				//out line
				pg.noFill();
				pg.stroke(0, opacity);
				pg.strokeWeight(1);
				pg.rect(left_x, p.by, lolly_size, lolly_size);

			}
		}


		//histogram grid
		pg.stroke(60);
		pg.strokeWeight(2);
		pg.line(h_axis_x, h_rect.y, h_axis_x, h_rect.y+h_rect.height);
		pg.fill(60);
		pg.textAlign(LEFT, CENTER);
		//grid
		for(int i = 1; i < last_pos+1; i++){
			if(i==1 || i%100 == 0){
				float gy = map(i, 1, last_pos, h_rect.y, h_rect.y+h_rect.height);
				pg.line(h_axis_x - 2,gy, h_axis_x+2, gy);
				// println("i="+i+"  gy="+gy);
				//label
				if(i==1 || i%200 == 0){
					pg.text(i, h_axis_x + 3, gy);
				}
			}
		}

		pg.endDraw();
	}

	public void draw(){
		background(255);
		textFont(small_font);
		int h_axis_x = h_rect.x+h_rect.width;

		if(selected_pop == null){
			image(opaque, 0, 0);
		}else{
			image(transparent, 0, 0);
			if(selected_pop != null){
				draw_active_pop();
			}
		}

		if(p_draw_counter > p_draw_max){
			println(" ---------  lollypop");
			noLoop();
			p_draw_counter = 0;
		}else{
			p_draw_counter ++;
		}
		
	}

	//draw selected pop
	public void draw_active_pop(){
		stroke(0);
		strokeWeight(1);
		noFill();
		beginShape();
		vertex(selected_pop.sx, selected_pop.sy);
		vertex(selected_pop.sx2, selected_pop.sy2);
		vertex(selected_pop.sx3, selected_pop.sy3);
		endShape();
		// line(l_rect.x, selected_pop.dy, l_rect.x+lolly_stick_length, selected_pop.dy);
		// ellipse(selected_pop.dx, selected_pop.dy, lolly_size, lolly_size);
		int num = selected_pop.mutations.size();
		float stripe_w = (float)lolly_size / (float)num;
		float left_x = (float)selected_pop.bx ;
		float runningX = left_x;
		for(Mutation m: selected_pop.mutations){
			fill(colors[m.type_index]);
			noStroke();
			rect(runningX, selected_pop.by, stripe_w, lolly_size);
			runningX += stripe_w; 
		}
		//out line
		noFill();
		stroke(0);
		strokeWeight(1);
		rect(left_x, selected_pop.by, lolly_size, lolly_size);

		//name
		textAlign(CENTER, BOTTOM);
		textFont(label_font);
		fill(0);
		String label = split(selected_pop.mut_name, "_")[1];
		text(label, selected_pop.bx, selected_pop.by);

	}


	public void mouseMoved(){
		cursor(ARROW);
		selected_pop = null;
		//check if it is hovering over any pop
		for(Pop p : data.pops){
			if(p.rect.contains(mouseX, mouseY)){
				cursor(HAND);
				selected_pop = p;
				// println("hovering: "+ p.mut_name);
				loop();

				update_main_view(p);

				// hovering_mutation = p.mutations.get(0);
				// parent.loop();

				return;
			}
		}
		loop();
		update_main_view(null);
	}
	public void mouseReleased(){}
	public void keyPressed(){}


}
class LollypopData {
	String name;
	ArrayList<Cell> cells;;
	ArrayList<Gene> genes;
	HashMap<String, Pop> pop_map; //mutation name
	ArrayList<Pop> pops;

	LollypopData(String name){
		this.name = name;
		this.cells = new ArrayList<Cell>();
		this.genes = new ArrayList<Gene>();
		this.pops = new ArrayList<Pop>();
		pop_map = new HashMap<String, Pop>();
	}

	public void addGene(Cell c, Gene g){
		cells.add(c);
		genes.add(g);

		//per mutation
		for(Mutation m : g.mutations){
			//find Pop
			Pop p = (Pop) pop_map.get(m.name);
			if(p == null){
				p = new Pop(g.name, m.name);
				p.pos = m.pos; //position -1 for unknown position
				pop_map.put(m.name, p);
				pops.add(p);
			}
			//add mutaion
			p.mutations.add(m);
			p.cells.add(c.name);
		}
	}

	public String toString(){
		String result = name+"\n";
		for(Pop p: pops){
			result += p.pos+" : "+p.cells+"\n";
		}
		return result;
	}

	public void setPositions(Rectangle rect, int last_pos, int lolly_size){
		float[] runningXs = new float[last_pos+1];
		int gap = 2;
		Arrays.fill(runningXs, 0);
		//set box position
		for(Pop p : pops){
			int pos = p.pos;
			if(pos == - 1){
				//unknown pos to off page
				p.bx = runningXs[0]+rect.x;
				runningXs[0]+= lolly_size+gap;
				p.by = rect.y + rect.height + (float)lolly_size/2f;
			}else{
				p.bx = runningXs[pos]+rect.x + lolly_stick_length;
				runningXs[pos] += lolly_size + gap;
				p.by = map(pos, 1, last_pos, rect.y, rect.y+rect.height);
			}
		}

		//set stick position
		//adjust positions
		float runningY = rect.y - lolly_size;
		int runningPos = 0;
		for(Pop p: pops){
			p.sx = rect.x;
			p.sy = p.by;
			//check if it is same position as the previous
			if(runningPos == p.pos){
				//same as previous position
				p.by = runningY;
				p.sx2 = rect.x +_margin;
				p.sy2 = runningY;
				p.sx3 = p.bx;
				p.sy3 = runningY;
			}else{
				//check if it overlaps
				if(p.by < runningY+ lolly_size){
					//overlaping
					p.by = runningY+lolly_size+gap;
					p.sx2 = rect.x +_margin ;
					p.sy2 = p.by;
					p.sx3 = p.bx;
					p.sy3 = p.by;
				}else{
					p.sx2 = rect.x +_margin;
					p.sy2 = p.by;
					p.sx3 = p.bx;
					p.sy3 = p.by;
				}
			}
			runningY = p.sy2;
			runningPos = p.pos;

			//make rectangle
			p.rect = new Rectangle(round(p.bx), round(p.by), lolly_size, lolly_size);
		}

	}
}
class Mutation{
	float rep;
	float con;
	String name;

	int pos;
	boolean is_hotspot = false;

	float ie = -1;
	boolean flag_ie = false; //change if the difference is large

	// String type = "NI"; //GOF, LOF, SI(silent), NI(not informative)
	int type_index = 3; //NI

	float dx, dy;

	Mutation(String name, float rep, float con){
		this.name = name;
		this.rep = rep;
		this.con = con;

		//parse name to get position
		String s[] = split(this.name, ".");
		if(s.length >1){
			String label = s[1];
			String[] match = match(label, "([A-Z])(\\d+)([A-Z]|del)");
			if(match !=  null){
				// println("name ="+label+" >> "+Arrays.toString(match)+" >> "+match[2]);
				this.pos = Integer.parseInt(match[2]);
			}else{
				match = match(label, "(\\d+)([_])(\\S+)");
				if(match != null){
					// println("match:"+match[1]);
					this.pos = Integer.parseInt(match[1]);
				}else{
					match = match(label, "([A-Z])(\\d+)(\\D+)");
					if(match != null){
						// println("match:"+match[2]);
						this.pos = Integer.parseInt(match[2]);
					}else{
						println("\tdebug:"+this.name +" >> "+label);
					}
					
				}
			}
		}else if(name.contains("_")){
			s = split(this.name,  "_");
			if(s.length > 1){
				String label = s[1];
				String[] match = match(label, "([A-Z])(\\d+)([A-Z]|del)");
				if(match !=  null){
					// println("name ="+this.name+" >> "+Arrays.toString(match)+" >> "+match[2]);
					this.pos = Integer.parseInt(match[2]);
				}else{
					this.pos = -1;
				}
			}
		}else{
			this.pos = -1;	
		}

		if(pos == -1){
			println("error: unable to parse mutation position:"+name);
		}
	}

	public String toString(){
		return name;
	}

}
class NumberBox{
	int dx, dy, dw, dh;
	Rectangle num_rect, up_rect, down_rect;
	float v_current, v_max, v_min;
	String name;
	String instruction;

	int decimal = 0;
	int text_color = 60;
	int active_color = color(5,113,176);


	NumberBox(int dx, int dy, int dw, int dh, float v_current, float v_max, float v_min, String name){
		this.dx = dx;
		this.dy = dy;
		this.dw = dw;
		this.dh = dh;
		this.v_current = v_current;
		this.v_max = v_max;
		this.v_min = v_min;
		this.name = name;

		float btn_size = (float)ceil(dh /2);
		num_rect = new Rectangle(dx, dy, round(dw-btn_size), dh);
		up_rect = new Rectangle(round(dx+dw-btn_size), dy, round(btn_size), round(btn_size));
		down_rect = new Rectangle(round(dx+dw-btn_size), round(dy+dh-btn_size), round(btn_size), round(btn_size));
	}


	public void render(){
		//instruction
		rectMode(CORNER);
		textAlign(LEFT, BOTTOM);
		fill(120);
		text(instruction, num_rect.x, num_rect.y);

		//draw box
		fill(255);
		stroke(180);
		strokeWeight(1);
		rect(num_rect.x, num_rect.y, num_rect.width, num_rect.height);
		rect(up_rect.x, up_rect.y, up_rect.width, up_rect.height);
		rect(down_rect.x, down_rect.y, down_rect.width, down_rect.height);
	
		// triangle
		if(up_rect.contains(mouseX, mouseY)){
			fill(color_active);
		}else{
			fill(180);
		}
		drawTraiange(up_rect, true);

		if(down_rect.contains(mouseX, mouseY)){
			fill(color_active);
		}else{
			fill(180);
		}
		drawTraiange(down_rect, false);

		fill(60);
		textAlign(RIGHT, CENTER);
		text(nf(v_current,0, decimal), num_rect.x+num_rect.width -2, (float)num_rect.getCenterY());
	}

	public void drawTraiange(Rectangle rect, boolean isUP){
		// fill(240);
		int dif = 2;
		noStroke();
		if(isUP){
			triangle((float) rect.getCenterX(), rect.y+dif, rect.x+rect.width -dif, rect.y+rect.height-dif, rect.x+dif, rect.y+rect.height-dif);
		}else{
			triangle((float) rect.getCenterX(), rect.y+rect.height-dif, rect.x+rect.width -dif, rect.y+dif, rect.x+dif, rect.y+dif);
		}
	}

	public boolean contains(int mx, int my){
		if(up_rect.x <= mx && mx <= up_rect.x+up_rect.width){
			if(up_rect.y <= my && my <= down_rect.y+down_rect.height ){
				return true;
			}
		}
		return false;
	}

	public void evaluate(int mx, int my, float increment){
		if(up_rect.contains(mx, my)){
			increase(increment);
		}else if(down_rect.contains(mx, my)){
			decrease(increment);
		}
	}

	public void increase(float n){
		this.v_current = min(v_current + n, v_max);
	}
	public void decrease(float n){
		this.v_current = max(v_current - n, v_min);
	}

}
//Lollypop
class Pop implements Comparable<Pop>{
	ArrayList<Mutation> mutations;
	ArrayList<String> cells;
	String gene_name;
	String mut_name;
	int pos;

	Rectangle rect;

	float sx, sy, sx2, sy2, sx3, sy3; //stick position
	float bx, by; //box position

	Pop(String gene_name, String mut_name){
		this.gene_name = gene_name;
		this.mut_name = mut_name;
		this.mutations = new ArrayList<Mutation>();
		this.cells = new ArrayList<String>();
	}

	public int compareTo(Pop that){
		if(this.pos < that.pos){
			return -1;
		}else if(this.pos > that.pos){
			return 1;
		}else{
			return 0;
		}
	}
}
class TextBox{
	String name;
	Rectangle area;
	String instruction;
	String text;
	Integer int_value = null;
	boolean isEditable = false;

	int active_color = color(5,113,176);


	TextBox(String name, int x, int y, int w, int h){
		this.name = name;
		this.area = new Rectangle(x, y, w, h);
		this.instruction = "";
		this.text = "";
	}

	public void render(){
		//instruction
		textAlign(LEFT, BOTTOM);
		fill(120);
		text(instruction, area.x, area.y);

		//draw box
		fill(255);
		stroke(180);
		strokeWeight(1);
		rect(area.x, area.y, area.width, area.height);

		//underline
		if(isEditable){
			if(area.contains(mouseX, mouseY)){
				stroke(active_color);
				line(area.x, area.y+area.height, area.x+area.width, area.y+area.height);
			}
		}
	
		//selected file name
		fill(60);
		textAlign(LEFT, CENTER);
		text(text, area.x+4, (float)area.getCenterY());
	}

	public void highlight(){
		stroke(active_color);
		strokeWeight(1);
		noFill();
		rect(area.x, area.y, area.width, area.height);
	}
}
class TextButton{
	String name;
	Rectangle area;
	String instruction;
	String text;
	boolean isSelected = false;


	TextButton(String name, int x, int y, int w, int h){
		this.name = name;
		this.area = new Rectangle(x, y, w, h);
		this.instruction = "";
		this.text = "";
		isSelected = false;
	}

	public void render(){
		//draw box
		fill(180);
		noStroke();
		rect(area.x, area.y, area.width, area.height);
		//text
		fill(area.contains(mouseX, mouseY)?color_active:255);
		textAlign(CENTER, CENTER);
		text(instruction, (float) area.getCenterX(), (float) area.getCenterY());
	}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "SparklerPlot" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
