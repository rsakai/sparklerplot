# README #


### How to prepare input files: ###
* For a configuration file, you can either edit the existing configuration file template or save a configuration file from the application. Please make sure that you do not change the column or row orders and labels. The file should be tab-delimited. 

    * `#plot_width` is the width of individual plot size in pixels.
    * `#plot_height` is the height of individual plot size in pixels.
    * `#x_axis` defines the label name and where ticks are drawn on the plot.
    * `#y_axis` defines the label name and where ticks are drawn on the y axis of the plot.
    * `#x_min` is the minimum value on the x-axis.
    * `#x_max` is the maximum value on the x-axis.
    * `#y_min` is the minimum value on the y-axis.
    * `#x_max` is the maximum value on the y-axis.
    * `input` is the url of the input file. In the current version (0.0.2), loading of only local files is supported. These urls can be either the absolute path, or if relative path from the location of configuration file. The file should include all the information, whether MI or CS, for all the cell lines. The order of cell lines in this file defines how each cell line is plotted on the screen, from left to right. 
    * `hotspot` is the url of the hotspot file directory. This needs to be a directory (folder) not a file. The folder should include text files for each gene, named as "KRAS.txt". The files in the sample data set are generated from counting the number of known mutations from the COSMIC dataset. For the format of these hotspot files, see the sample file and make them tab-delimited. This is an optional setting, thus you do not have to have the hotspot files to run the application. 
    * `ie` is the url for the infection efficiency files. This file includes, gene name, mutation name, and  IE score for each cell line. The wild type mutation is indicated with "_WT" in the mutation name.This setting is also optional.
    * `cell` defines the name for each cell line and thresholds for the robustness and connectivity. These cell line names should be consistent across different data sets used. The thresholds for the robustness and connectivity are defined in this order. Add another row to include other cell line data.

### How data are processed ###

* 

### How to run the application ###
1. Select the input file. ![Screen Shot 2014-08-11 at 18.15.33.png](https://bitbucket.org/repo/M7jkey/images/1106782733-Screen%20Shot%202014-08-11%20at%2018.15.33.png)
2. Then, define thresholds for each cell line.  When you select an input file, the application scans the cell line names and adjust the interface to enable to adjust threshold values.
![Screen Shot 2014-08-11 at 18.15.54.png](https://bitbucket.org/repo/M7jkey/images/2932772003-Screen%20Shot%202014-08-11%20at%2018.15.54.png)
3. Optinally, select hotspot file directory and infection efficiency file. ![Screen Shot 2014-08-11 at 18.16.08.png](https://bitbucket.org/repo/M7jkey/images/1192367313-Screen%20Shot%202014-08-11%20at%2018.16.08.png)![Screen Shot 2014-08-11 at 18.16.21.png](https://bitbucket.org/repo/M7jkey/images/1365348938-Screen%20Shot%202014-08-11%20at%2018.16.21.png)
4. If you wish to save the current configuration, you can save the current configuration by clicking on the `save`.
5. Hit `load` to start loading and processing the data files.




### Interaction ###

* press `p` to export a pdf. The file is saved in the same directory as the application.
* press `l` to toggle between showing or hiding labels for each mutation.
* mouse over a mutation to highlight.
* click on the gene name in the main view to open a flag plot on another window. This function is supported only when the hotspot files are specified.



### Running the App from Processing ###

* This application was developed using the Processing.  You can download Processing for free: [https://www.processing.org/download/?processing](here) 
* Then, go to the "downloads" page ([https://bitbucket.org/rsakai/sparklerplot/downloads](here)), and click on the text that says "Download repository", or click [https://bitbucket.org/rsakai/sparklerplot/get/a5004cc4a7db.zip](here).  You should have a zip file downloaded on your computer.  And unzip the zip file. 
* Inside the folder, you should find a file named "SparklerPlot.pde".  Double click this file, or select this file from Processing. When you open the file in Processing, it should look like as follows:

![Screen Shot 2014-11-07 at 11.48.13.png](https://bitbucket.org/repo/M7jkey/images/1531404895-Screen%20Shot%202014-11-07%20at%2011.48.13.png)

* If you click on the play button, the application should run.  However, if it encounters a bug or error, the error messages are printed in the Console.  Please send the error message to the developer to fix the bug. 

![Screen Shot 2014-11-07 at 11.48.13.png](https://bitbucket.org/repo/M7jkey/images/1951309667-Screen%20Shot%202014-11-07%20at%2011.48.13.png) 



