class LollypopData {
	String name;
	ArrayList<Cell> cells;;
	ArrayList<Gene> genes;
	HashMap<String, Pop> pop_map; //mutation name
	ArrayList<Pop> pops;

	LollypopData(String name){
		this.name = name;
		this.cells = new ArrayList<Cell>();
		this.genes = new ArrayList<Gene>();
		this.pops = new ArrayList<Pop>();
		pop_map = new HashMap<String, Pop>();
	}

	void addGene(Cell c, Gene g){
		cells.add(c);
		genes.add(g);

		//per mutation
		for(Mutation m : g.mutations){
			//find Pop
			Pop p = (Pop) pop_map.get(m.name);
			if(p == null){
				p = new Pop(g.name, m.name);
				p.pos = m.pos; //position -1 for unknown position
				pop_map.put(m.name, p);
				pops.add(p);
			}
			//add mutaion
			p.mutations.add(m);
			p.cells.add(c.name);
		}
	}

	String toString(){
		String result = name+"\n";
		for(Pop p: pops){
			result += p.pos+" : "+p.cells+"\n";
		}
		return result;
	}

	void setPositions(Rectangle rect, int last_pos, int lolly_size){
		float[] runningXs = new float[last_pos+1];
		int gap = 2;
		Arrays.fill(runningXs, 0);
		//set box position
		for(Pop p : pops){
			int pos = p.pos;
			if(pos == - 1){
				//unknown pos to off page
				p.bx = runningXs[0]+rect.x;
				runningXs[0]+= lolly_size+gap;
				p.by = rect.y + rect.height + (float)lolly_size/2f;
			}else{
				p.bx = runningXs[pos]+rect.x + lolly_stick_length;
				runningXs[pos] += lolly_size + gap;
				p.by = map(pos, 1, last_pos, rect.y, rect.y+rect.height);
			}
		}

		//set stick position
		//adjust positions
		float runningY = rect.y - lolly_size;
		int runningPos = 0;
		for(Pop p: pops){
			p.sx = rect.x;
			p.sy = p.by;
			//check if it is same position as the previous
			if(runningPos == p.pos){
				//same as previous position
				p.by = runningY;
				p.sx2 = rect.x +_margin;
				p.sy2 = runningY;
				p.sx3 = p.bx;
				p.sy3 = runningY;
			}else{
				//check if it overlaps
				if(p.by < runningY+ lolly_size){
					//overlaping
					p.by = runningY+lolly_size+gap;
					p.sx2 = rect.x +_margin ;
					p.sy2 = p.by;
					p.sx3 = p.bx;
					p.sy3 = p.by;
				}else{
					p.sx2 = rect.x +_margin;
					p.sy2 = p.by;
					p.sx3 = p.bx;
					p.sy3 = p.by;
				}
			}
			runningY = p.sy2;
			runningPos = p.pos;

			//make rectangle
			p.rect = new Rectangle(round(p.bx), round(p.by), lolly_size, lolly_size);
		}

	}
}
