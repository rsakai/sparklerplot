String app_title = "Sparkler Plot";
int app_title_x, app_title_y;
PFont title_font;

//box parameters
int btn_height = _margin*2;
int btn_width = _margin*10;
int nb_width = _margin*5;
int fileName_width = _margin*30;
int btn_gap = _margin*2;

ArrayList<TextBox> textboxes;
ArrayList<TextButton> textbtns;
ArrayList<String> texts;
ArrayList<PVector> text_positions;

// ArrayList<NumberBox> numberboxes = null;

ArrayList<NumberBox> con_numberboxes, rob_numberboxes;

//cell line
// ArrayList<String> unique_cell_line = null;


//hovering
int color_active = c_blue;

//File
File input_file = null;
File hotspot_file = null;
File ie_file = null;

//TextBox
TextBox input_box, hotspot, ie, conf_load, conf_save;
TextButton hotspot_btn, ie_btn, conf_load_btn, conf_save_btn, load_btn;


//Status
boolean isError = false;
String error_msg = "";

//Optional data
boolean isHotspotData = false;
boolean isIEData = false;


void setup_data_loading_page(){
	textboxes = new ArrayList<TextBox>();
	textbtns = new ArrayList<TextButton>();
	texts = new ArrayList<String>();
	text_positions = new ArrayList<PVector>();
	// numberboxes = new ArrayList<NumberBox>();
	rob_numberboxes = new ArrayList<NumberBox>();
	con_numberboxes = new ArrayList<NumberBox>();
	

	int runningX = _left_margin;
	int runningY = _top_margin;
	//title
	app_title_x = runningX;
	app_title_y = runningY;
	title_font = createFont("SansSerif", 30);
	runningY += _margin * 8;

	//input file
	input_box = new TextBox("input", runningX, runningY, fileName_width, btn_height);
	input_box.instruction = "Please select an input file:";
	textboxes.add(input_box);
	//input btn
	TextButton input_btn = new TextButton("input_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	input_btn.instruction = "select";
	textbtns.add(input_btn);
	runningY += btn_height + btn_gap + _margin*2;


	//optional files
	//hotspot
	hotspot = new TextBox("hotspot", runningX, runningY, fileName_width, btn_height);
	hotspot.instruction ="Optional: Select a hotspot file directory:";
	textboxes.add(hotspot);
	hotspot_btn = new TextButton("hotspot_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	hotspot_btn.instruction = "select";
	textbtns.add(hotspot_btn);
	runningY += btn_height + btn_gap;

	//ie
	ie = new TextBox("ie", runningX, runningY, fileName_width, btn_height);
	ie.instruction ="Optional: Select a infection efficiency file:";
	textboxes.add(ie);
	ie_btn = new TextButton("ie_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	ie_btn.instruction = "select";
	textbtns.add(ie_btn);
	runningY += btn_height + btn_gap;

	//load configuration file
	conf_load = new TextBox("conf_load", runningX, runningY, fileName_width, btn_height);
	conf_load.instruction ="Optional: Load a configuration file:";
	textboxes.add(conf_load);
	conf_load_btn = new TextButton("conf_load_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	conf_load_btn.instruction = "select";
	textbtns.add(conf_load_btn);
	runningY += btn_height + btn_gap;

	//save configuration
	conf_save = new TextBox("conf_save", runningX, runningY, fileName_width, btn_height);
	conf_save.instruction ="Optional: Save a configuration file:";
	conf_save.text = "conf_"+timestamp_detail()+".txt";
	textboxes.add(conf_save);
	conf_save_btn = new TextButton("conf_save_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	conf_save_btn.instruction = "save";
	textbtns.add(conf_save_btn);
	runningY += btn_height + btn_gap;

	//load btn
	load_btn = new TextButton("load_btn", runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	load_btn.instruction ="load";
	textbtns.add(load_btn);
	runningY += btn_height +btn_gap;

}

void update_data_loading_page(){
	println("debug ---- start of update_data_loading_page() ----");
	int runningX = _left_margin;
	int runningY = _top_margin;
	texts = new ArrayList<String>();
	text_positions = new ArrayList<PVector>();
	//title
	app_title_x = runningX;
	app_title_y = runningY;
	title_font = createFont("SansSerif", 30);
	runningY += _margin * 8;

	//input file
	//input btn
	runningY += btn_height + btn_gap + _margin*2;

	//cell line
	texts.add("cell line");
	text_positions.add(new PVector(runningX, runningY));
	texts.add("thresholds");
	text_positions.add(new PVector(runningX + btn_width +_margin*4, runningY));
	runningY += btn_height +_margin;

	//cell line number boxes
	// numberboxes = new ArrayList<NumberBox>();
	rob_numberboxes = new ArrayList<NumberBox>();
	con_numberboxes = new ArrayList<NumberBox>();
	con_numberboxes = new ArrayList<NumberBox>();


	for(int i = 0; i< conf.cell_lines.size(); i++){
		String cell = conf.cell_lines.get(i);
		// println("debug:cell line:"+cell+"  pos:"+runningX+"  "+runningY);
		texts.add(cell);
		text_positions.add(new PVector(runningX, runningY));
		//robustness
		NumberBox robustness = new NumberBox(runningX+btn_width+_margin*4, runningY, nb_width, btn_height, conf.robustness.get(i), 100, 0, cell+"_rob");
		robustness.instruction = "robustness";
		rob_numberboxes.add(robustness);

		//connectivity
		NumberBox connectivity = new NumberBox(runningX+btn_width+nb_width+btn_gap+_margin*5, runningY, nb_width, btn_height, conf.connectivity.get(i), 100, 0, cell+"_con");
		connectivity.instruction = "connectivity";
		con_numberboxes.add(connectivity);
		
		runningY += btn_height + btn_gap;
	}

	//position optional files options
	runningY += _margin*2;
	hotspot.area = new Rectangle(runningX, runningY, fileName_width, btn_height);
	hotspot_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height + btn_gap;

	ie.area = new Rectangle(runningX, runningY, fileName_width, btn_height);
	ie_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height + btn_gap;

	conf_load.area = new Rectangle(runningX, runningY, fileName_width, btn_height);
	conf_load_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height + btn_gap;

	conf_save.area = new Rectangle(runningX, runningY, fileName_width, btn_height);
	conf_save_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height + btn_gap;

	//load btn
	load_btn.area = new Rectangle(runningX+fileName_width+_margin, runningY, btn_width, btn_height);
	runningY += btn_height +btn_gap;

	println("debug ---- end of update_data_loading_page() ----");
}

TextBox getTextBox(String name){
	for(TextBox tb: textboxes){
		if(tb.name.equals(name)){
			return tb;
		}
	}
	return null;
}

//while file is being selected
void draw_data_loading_page(){

	//draw title
	textFont(title_font);
	fill(120);
	textAlign(LEFT, TOP);
	text(app_title, app_title_x, app_title_y);

	//input
	textFont(font);
	//text boxes
	for(TextBox tb: textboxes){
		tb.render();
	}
	//text btns
	for(TextButton tb: textbtns){
		tb.render();
	}

	if(rob_numberboxes != null){
		for(NumberBox nb: rob_numberboxes){
			nb.render();
		}
	}
	if(con_numberboxes != null){
		for(NumberBox nb: con_numberboxes){
			nb.render();
		}
	}

	//text labels
	fill(120);
	textAlign(LEFT, TOP);
	for(int i = 0; i < texts.size(); i++){
		PVector pos = text_positions.get(i);
		text(texts.get(i),pos.x, pos.y);
	}		
}


//input file is selected
void select_input_file(File selection){
	if(selection == null){
		input_file = null;
		input_box.text = "";
	}else{
		input_file = selection;
		String url = selection.getAbsolutePath();
		conf.input_file_url = url;
		if(url.length() > 40){
			input_box.text = "..."+url.substring((url.length()-37), (url.length()));
		}else{
			input_box.text = url;
		}

		// pre parse the file
		// find the number of unique cell lines
		String[] lines = loadStrings(selection);

		for(int i = 0; i < lines.length; i++){
			String l = lines[i];
			String[] s = split(l, "\t");
			if(i==0){
				//header
				if(s.length < 7){
					isError = true;
					error_msg = "Please check your input file structure.";
				}else if(!s[0].toLowerCase().startsWith("gene")){
					isError = true;
					error_msg = "Please check your input file structure.";
				}else if(!s[6].toLowerCase().startsWith("cell_line")){
					isError = true;
					error_msg = "Please check your input file structure.";
				}

				if(isError){
					break;
				}
			}else{
				String cell_line = s[6].trim();
				if(!conf.cell_lines.contains(cell_line)){
					conf.cell_lines.add(cell_line);
				}
			}
		}
	}

	if(conf.cell_lines.size() != conf.robustness.size()){
		conf.robustness = new ArrayList<Integer>();
		conf.connectivity = new ArrayList<Integer>();
		//set default
		for(String c:conf.cell_lines){
			conf.robustness.add(73);
			conf.connectivity.add(90);
		}
	}

	// println("MI file is scanned: "+conf.cell_lines);
	update_data_loading_page();
	loop();
}

void download_input_file(String filename){
	try {
		URL url = new URL(filename);
		InputStream stream = url.openStream();

		if(filename.length() > 40){
			input_box.text = "..."+filename.substring((filename.length()-37), (filename.length()));
		}else{
			input_box.text = filename;
		}

		// pre parse the file
		// find the number of unique cell lines
		String[] lines = loadStrings(stream);
		println("debug:"+Arrays.toString(lines));

		for(int i = 0; i < lines.length; i++){
			String l = lines[i];
			String[] s = split(l, "\t");
			if(i==0){
				//header
				if(s.length < 7){
					isError = true;
					error_msg = "Please check your input file structure.";
				}else if(!s[0].toLowerCase().startsWith("gene")){
					isError = true;
					error_msg = "Please check your input file structure.";
				}else if(!s[6].toLowerCase().startsWith("cell_line")){
					isError = true;
					error_msg = "Please check your input file structure.";
				}

				if(isError){
					break;
				}
			}else{
				String cell_line = s[6].trim();
				if(!conf.cell_lines.contains(cell_line)){
					conf.cell_lines.add(cell_line);
				}
			}
		}
		if(conf.cell_lines.size() != conf.robustness.size()){
			conf.robustness = new ArrayList<Integer>();
			conf.connectivity = new ArrayList<Integer>();
			//set default
			for(String c:conf.cell_lines){
				conf.robustness.add(73);
				conf.connectivity.add(90);
			}
		}

		println("MI file is scanned: "+conf.cell_lines);
		update_data_loading_page();
		loop();
	} catch (MalformedURLException mfue) {
		// not a url, that's fine
	} catch (IOException e) {
		// changed for 0117, shouldn't be throwing exception
		e.printStackTrace();
		//System.err.println("Error downloading from URL " + filename);
	}
}


//hotspot folder is selected
void select_hotspot_folder(File selection){
	println("select_hotpost_folder():"+selection.getAbsolutePath());
	if(selection == null){
		hotspot_file = null;
		isHotspotData = false;
	}else{
		//check if it is a directory
		if(selection.isDirectory()){
			hotspot_file = selection;
			isHotspotData = true;
			String url = selection.getAbsolutePath();
			conf.hotspot_folder_url = url;
			if(url.length() > 40){
				hotspot.text = "..."+url.substring((url.length()-37), (url.length()));
			}else{
				hotspot.text = url;
			}
		}else{
			try {
				//pop up
				javax.swing.JOptionPane.showMessageDialog(null, "Please select a directory.");
			} catch (Exception e) {
			   println("error on load_btn click:"+e);
			}
		}
	}
	loop();
}


void select_ie_file(File selection){
	if(selection == null){
		ie_file = null;
		isIEData = false;
	}else{
		ie_file = selection;
		isIEData = true;
		String url = selection.getAbsolutePath();
		conf.ie_file_url = url;
		if(url.length() > 40){
			ie.text = "..."+url.substring((url.length()-37), (url.length()));
		}else{
			ie.text = url;
		}
	}
	loop();
}


void select_conf_load(File selection){
	if(selection == null){
	}else{
		String url = selection.getAbsolutePath();
		conf.local_url = selection.getParent();
		// println("debug: file is selected:"+conf.local_url);

		if(url.length() > 40){
			conf_load.text = "..."+url.substring((url.length()-37), (url.length()));
		}else{
			conf_load.text = url;
		}

		//parse configuration
		// String msg = "";

		String[] lines = loadStrings(selection);
		Configuration new_conf = new Configuration(lines, conf.local_url);
		if(new_conf.isError){
			try {
				//pop up
				javax.swing.JOptionPane.showMessageDialog(null, "Error with loading the configuration file:\n"+new_conf.msg);
			} catch (Exception e) {
			   println("error on load_btn click:"+e);
			}
		}else{
			conf = new_conf;
			//update
			update_data_loading_page();
		}
	}


	loop();
}

void select_conf_save(File selection){
	if(selection == null){
	}else{
		if(selection.isDirectory()){
			String url = selection.getAbsolutePath()+"/"+conf_save.text;
			//get configuration output
			String output = conf.exportConfiguration();
			// println("output ::::: \n"+output);
			//save a text file

			PrintWriter writer = createWriter(url);
			writer.println(output);
			writer.flush();
			writer.close();
			// println("debug: save url:"+url);
			try {
				//pop up
				javax.swing.JOptionPane.showMessageDialog(null, conf_save.text+" is saved.");
			} catch (Exception e) {
			   println("error on configuration save click:"+e);
			}


		}else{
			try {
				//pop up
				javax.swing.JOptionPane.showMessageDialog(null, "Please select a directory.");
			} catch (Exception e) {
			   println("error on load_btn click:"+e);
			}
		}
	}
}

