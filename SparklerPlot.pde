
import java.awt.event.*;
import java.awt.*;

import java.util.*;
import javax.swing.*;
import processing.pdf.*;

import java.net.*;
import java.io.FileNotFoundException;


//display
int _margin = 10;
int _top_margin = _margin*5;
int _bottom_margin = _margin *5;
int _left_margin = _margin*10;
int _right_margin = _margin*7;
int _banner_height = 50;
int _h_gap = _margin*2;
int _v_gap = _margin*5; 
int _tick = 2;

int _wt_gap = _margin;

int stage_height, stage_width;

int n_row = 5;
int n_col = 5; // number of cell line

Rectangle legend_rect;


//data
HashMap<String, Gene> gene_map;
ArrayList<Gene> all_genes;
HashMap<String, Cell> cell_map;
ArrayList<Cell> all_cells;

ArrayList<String> gene_functions;

//dashed line 
float[] dash_line = {1f, 2f};

// //threshod
float x_threshold = 73f;
float y_threshold = 90f;


PFont font; 
PFont large_font;// = createFont("SansSerif", 12);
PFont small_font;// = createFont("SansSerif", 8);
PFont label_font;// = createFont("pf_tempesta_seven.ttf", 7);

//flags
boolean record = false;
boolean show_labels = false;
//stages
boolean isFileSelected = false;
boolean isFileLoading = false;
boolean isFileLoaded = false;
//data
boolean include_IE = false;
boolean include_hotspot = false;


String[] mu_types = {"GOF", "LOF", "Silence", "NotInformative"};

//scroll bar
Rectangle scroll_area, scroll_bar, scroll_bg;
int image_y_pos = 0;
boolean isScrolling = false;
int m_pressed_y;

//color coding
int c_red = color(202,0,32);
int c_blue = color(5,113,176);
int c_light_gray = 200;
int c_dark_Gray = 60;
int[] colors = {c_red, c_blue, c_dark_Gray, c_light_gray};


PGraphics p;
PGraphics p_trans;
int transparency = 20;

PImage target_cursor;
Cell hovering_cell = null;
Gene hovering_gene = null;
Mutation hovering_mutation = null;
boolean hovering_wt = false;


int draw_counter = 0;
int draw_max = 60; //2 seconds = draw 60 times before it stops


//Lollypop
float pos_weight = 0.5f;
int lolly_stick_length = _margin*4;

//gene name hit area
Rectangle[] gene_hit_area;
Gene hovering_gene_name = null;

//Configuration
Configuration conf;


void setup(){
	stage_width = displayWidth;
	stage_height = displayHeight - _banner_height;
	size(stage_width, stage_height);
	//font
	font = createFont("SansSerif", 12);
	large_font = createFont("SansSerif", 12);
	small_font = createFont("SansSerif", 8);
	label_font = createFont("SansSerif", 10);
	textFont(font);

	//set up configuration
	conf = new Configuration();


	//data loading page
	setup_data_loading_page();
}



void start_loading(){
	isFileSelected = true;
	isFileLoading = true;
	//load data
	cell_map = new HashMap<String, Cell>();
	gene_map = new HashMap<String, Gene>();
	all_genes = new ArrayList<Gene>();
	gene_functions = new ArrayList<String>();
	all_cells = new ArrayList<Cell>();

	load_data(conf.input_file_url);

	//Infection Efficiency data
	if(include_IE){
		println("loading IE file...");
		load_ie_list(conf.ie_file_url);
	}

	//hotspot information
	if(include_hotspot){
		println("loading Hotspot file...");
		load_hotspot(conf.hotspot_folder_url);
	}

	//cursor
	target_cursor = loadImage("cursor.png");


	println("total number of genes = "+all_genes.size());
	println("gene functions = "+gene_functions);
	println("cell lines = "+ all_cells);

	n_col = all_cells.size();
	//sort genes
	// Collections.sort(all_genes, new GeneComparator("name"));
	Collections.sort(all_genes, new GeneComparator("class"));
	
	//parse mutation
	parse_mutations();
	//setup display
	setup_display();

	//draw
	update_display(p, 255);
	update_display(p_trans, transparency);

	smooth();
	noLoop();
	frameRate(25);

	//end of loading
	isFileLoading = false;
	isFileLoaded = true;
}


void setup_display(){
	//stage size
	n_row = all_genes.size();
	stage_width = _left_margin + _right_margin + n_col*conf.box_w + (n_col-1)*_h_gap;
	stage_height = _top_margin + _bottom_margin + n_row*conf.box_h + (n_row-1)*_v_gap;
	size(stage_width, displayHeight - _banner_height);
	println(stage_width +" x "+ stage_height);

	p = createGraphics(stage_width, stage_height);
	p_trans = createGraphics(stage_width, stage_height);
	
	//adjust frame size
	frame.setResizable(true);
	frame.setSize(stage_width, displayHeight-_banner_height);



	//set rectangle for each gene
	int row_counter = 0;
	int col_counter = 0;
	int runningY = _top_margin;

	//gene name hit area
	gene_hit_area = new Rectangle[all_genes.size()];
	int counter = 0;
	for(Gene g: all_genes){
		g.rects = new Rectangle[n_col];
		int runningX = _left_margin;
		//name area
		gene_hit_area[counter] = new Rectangle(0, runningY - _wt_gap, _left_margin, 15);

		for(int i = 0; i < n_col; i++){
			g.rects[i] = new Rectangle(runningX, runningY, conf.box_w, conf.box_h);
			runningX += conf.box_w + _h_gap;
		}
		runningY += conf.box_h + _v_gap;
		counter ++;
	}
	int runningX = _left_margin;
	//legend
	legend_rect = new Rectangle(runningX, runningY, conf.box_w, conf.box_h);

	//scroll bar
	scroll_bg = new Rectangle(stage_width - _margin, 0, _margin, (displayHeight-_banner_height));
	scroll_area = new Rectangle(scroll_bg.x, scroll_bg.y+_margin/2, _margin, (displayHeight-_banner_height)-_margin);
	int bar_height = round(map((displayHeight-_banner_height), 0, stage_height, 0, scroll_area.height));
	int bar_y_1 = _margin/2;
	scroll_bar = new Rectangle(scroll_area.x, scroll_area.y, scroll_area.width, bar_height);
	image_y_pos = 0;
}

void update_main_view(Pop p){
	if(p== null){
		hovering_mutation = null;
		hovering_gene = null;
	}else{
		hovering_mutation = p.mutations.get(0);
		String cell_name = p.cells.get(0);
		Cell c = (Cell) cell_map.get(cell_name);
		hovering_gene = (Gene) c.gene_map.get(p.gene_name);
	}
	loop();
}

void draw(){
	if(record){
		beginRecord(PDF, timestamp()+".pdf");
	}
	background(240);

	if(!isFileSelected){
		//draw data loading page
		draw_data_loading_page();
	}else{
		if(isFileLoading){
			//while data is loading
			textAlign(CENTER, CENTER);
			fill(120);
			text("Loading data ... ", displayWidth/2, displayHeight/2);
		}else if(isFileLoaded){
			// image(p, 0, image_y_pos);
			if(hovering_mutation == null){
				image(p, 0, image_y_pos);
			}else{
				image(p_trans, 0, image_y_pos);
			}

			//draw hovering item
			draw_interaction();
			draw_scroll_bar();
		}
	}
	if(record){
		endRecord();
		record = false;
	}

	if(draw_counter > draw_max){
		println(" ---------");
		noLoop();
		draw_counter = 0;
	}else{
		draw_counter ++;
	}

}



//draw hovering item
void draw_interaction(){
	if(hovering_gene != null){
		Gene hg = hovering_gene;
		if(hovering_wt){
			//hovering wt 
			// println("debug: hovering wt");
		}else{
			if(hovering_mutation != null){
				Mutation hm = hovering_mutation;
				//high light hovering mutation
				float wt_x = hg.wt_dx;
				float wt_y = hg.wt_dy + image_y_pos;
				float m_x = hm.dx;
				float m_y = hm.dy + image_y_pos;

				stroke(colors[hm.type_index]);
				strokeWeight(1);
				if(!hm.flag_ie){
					line(wt_x, wt_y, m_x, m_y);
				}else{
					dashline(wt_x, wt_y, m_x, m_y, dash_line);
				}
				fill(colors[hm.type_index]);
				noStroke();
				ellipse(m_x, m_y, 3, 3);
				//hot spot
				if(hm.is_hotspot){
					strokeWeight(0.5);
					stroke(colors[hm.type_index]);
					noFill();
					ellipse(m_x, m_y, 5, 5);
				}
				


				//draw label
				textFont(label_font);
				textAlign(CENTER, CENTER);
				String label = "  "+split(hm.name, "_")[1]+"  ";
				float textWidth = textWidth(label);
				float textHeight = 15;
				fill(255, 180);
				noStroke();
				rect(m_x, m_y, textWidth, textHeight);
				fill(0);
				text(label, m_x + textWidth/2, m_y+textHeight/2);



				//highlight mutations on other cell line
				for(Cell c: all_cells){
					if(c != hovering_cell){
						Gene g = (Gene)c.gene_map.get(hg.name);
						if(g != null){
							//check if the mutation exist
							String m_name = hm.name;
							Mutation m = g.findMutation(m_name);
							if(m != null){
								//draw these lines
								float wt_x_2 = g.wt_dx;
								float wt_y_2 = g.wt_dy + image_y_pos;
								float m_x_2 = m.dx;
								float m_y_2 = m.dy + image_y_pos;
								stroke(colors[m.type_index]);
								strokeWeight(1);
								if(!m.flag_ie){
									line(wt_x_2, wt_y_2, m_x_2, m_y_2);
								}else{
									dashline(wt_x_2, wt_y_2, m_x_2, m_y_2, dash_line);
								}

								fill(colors[m.type_index]);
								noStroke();
								ellipse(m_x_2, m_y_2, 3, 3);
								//hot spot
								if(m.is_hotspot){
									strokeWeight(0.5);
									stroke(colors[m.type_index]);
									noFill();
									ellipse(m_x_2, m_y_2, 5, 5);
								}

								//draw label
								textFont(label_font);
								textAlign(CENTER, CENTER);
								// fill(255, 120);
								// noStroke();
								// rect(m_x_2, m_y_2, textWidth, textHeight);
								fill(0);
								text(label, m_x_2 + textWidth/2, m_y_2+textHeight/2);

							}
						}
					}
				}
			}else{
				// println("debug: mutation is null");
			}
		}
	}else{
		// println("\tdebug: hovering_gene is null");
	}

	if(hovering_gene_name != null){
		//highlight gene name
		fill(c_red);
		Rectangle rect = hovering_gene_name.rects[0];
		textFont(large_font);
		textAlign(RIGHT, TOP);
		text(hovering_gene_name.name, rect.x-_margin*2.5, rect.y - _wt_gap);

	}
}

void draw_scroll_bar(){
	strokeCap(ROUND);
	strokeWeight(_margin);
	if(scroll_bar.contains(mouseX, mouseY)){
		stroke(120);
	}else{
		stroke(180);
	}
	line((float)scroll_bar.getCenterX(), scroll_bar.y, (float)scroll_bar.getCenterX(), scroll_bar.y+scroll_bar.height);
}

//update PGraphics
void update_display(PGraphics pg, int opacity){
	pg.beginDraw();
	pg.smooth();
	pg.background(240);
	pg.textFont(large_font);

	float runningX = _left_margin;
	for(int i = 0; i < all_cells.size(); i++){
		Cell c = all_cells.get(i);
		//draw cell line name
		Gene first_g = all_genes.get(0);
		String label = split(c.name, "_")[0];
		pg.fill(120);
		pg.textAlign(CENTER, TOP);
		pg.text(label, (float)first_g.rects[i].getCenterX(), _margin*2);

		for(int k = 0; k < all_genes.size(); k++){
			Gene g = all_genes.get(k);
			Rectangle rect = g.rects[i];
			Gene gene = (Gene) c.gene_map.get(g.name);
			if(gene != null){
				gene.renderBackground(pg, rect, i);
				gene.renderPlot(pg, rect, opacity);
			}

			//gene label
			if(i == 0 ){
				pg.textAlign(RIGHT, TOP);
				//gene name
				pg.fill(0);
				pg.text(g.name, rect.x-_margin*2.5, rect.y - _wt_gap);
				//gene function
				pg.fill(120);
				pg.text(g.func, rect.x-_margin*2.5, rect.y+_margin*1.5- _wt_gap);

				if(!g.g_class.equals("NA")){
					pg.fill(120);
					pg.text(g.g_class, rect.x-_margin*2.5, rect.y+_margin*3- _wt_gap);

				}
				if(gene !=null){
					//x 
					pg.textFont(small_font);
					pg.textAlign(CENTER, TOP);
					for(int j :conf.x_axis_ticks){
						float dx = map(j, conf.x_min, conf.x_max, 0, conf.box_w) + rect.x;
						pg.fill(180);
						pg.text(j, dx, rect.y+rect.height);
						//tick marks
						pg.stroke(160);
						pg.strokeWeight(1);
						pg.line(dx, rect.y+rect.height, dx, rect.y+rect.height -2);
					}
					//axis label
					pg.fill(180);
					pg.text(conf.x_axis_label, (float)rect.getCenterX(), rect.y+rect.height+_margin);

					//y
					pg.textAlign(RIGHT, CENTER);
					for(int j: conf.y_axis_ticks){
						float dy = map(j, conf.y_min, conf.y_max, conf.box_h, 0) + rect.y;
						pg.fill(180);
						pg.text(j, rect.x-2, dy);
						//tick marks
						pg.stroke(160);
						pg.strokeWeight(1);
						pg.line(rect.x, dy, rect.x+2, dy);

					}
					//wt label
					pg.fill(180);
					pg.text("WT", rect.x-2, rect.y - _margin-2);
					//label
					pg.pushMatrix();
					pg.translate(rect.x-_margin*1.5, (float)rect.getCenterY());
					pg.rotate(-PI/2);
					pg.textAlign(CENTER, BOTTOM);
					pg.text(conf.y_axis_label, 0, 0);
					pg.popMatrix();
					pg.textFont(large_font);
				}
			}
			if(k != 0 && gene != null){ //skip the first gene
				//celll line
				pg.textFont(small_font);
				pg.textAlign(CENTER, BOTTOM);
				pg.fill(180);

				String c_label = split(c.name, "_")[0];
				// pg.text(c_label, (float)rect.getCenterX(),rect.y - _wt_gap);
				pg.textFont(large_font);
			}
			
		}
		runningX += conf.box_w +_h_gap;
	}
	pg.endDraw();
}



String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

String timestamp_detail() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$tm%1$td%1$ty_%1$tH%1$tM", now);
}

void createLollypop(String gene_name){
	Frame f = new Frame(gene_name);
	Lollypop lp = new Lollypop(this, gene_name, f, conf.hotspot_folder_url+"/"+gene_name+".txt");
	f.add(lp);
	lp.init();
	f.setTitle(gene_name);
	f.setSize(lp.w, lp.h);
	f.setLocation(0,0);
	f.setResizable(false);
	f.setVisible(true);

	f.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent windowEvent){
	    	Frame frame = (Frame)windowEvent.getComponent();
	    	frame.setVisible(false);
	    	frame.dispose();
	    	println("close!");
		}        
	});    
	
}

//create data object for lollypop
LollypopData getLollypopData(String name){
	LollypopData result = new LollypopData(name);
	//for each cell line
	for(int i = 0; i < all_cells.size(); i++){
		Cell c = all_cells.get(i);	
		//see if the gene exist
		Gene gene = (Gene) c.gene_map.get(name);
		if(gene != null){
			result.addGene(c, gene);
		}
	}
	Collections.sort(result.pops);
	return result;
}

