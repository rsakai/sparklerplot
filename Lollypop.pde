public class Lollypop extends PApplet {
	String gene_name;
	PApplet parent;
	Frame f;
	int w, h;
	int l_margin = _margin*3;
	int r_margin = _margin*3;
	int hist_width = _margin*10;
	int lolly_width = _margin*10;
	int lolly_size = 8;

	int[] histogram;
	int last_pos;
	int max_count;

	Rectangle h_rect, l_rect;
	LollypopData data;

	PGraphics opaque, transparent;


	Pop selected_pop = null;

	int p_draw_counter = 0;
	int p_draw_max = 30;

	private Lollypop(){}
	private Lollypop(PApplet theParent, String gene_name, Frame f, String url){
		this.parent = theParent;
		this.gene_name = gene_name;
		this.f = f;
		//load data
		String[] lines = loadStrings(url);
		for(String l: lines){
			if(l.startsWith("name")){
				//name
			}else if(l.startsWith("last_pos")){
				//last position
				String[] s = split(l, "\t");
				last_pos = Integer.parseInt(s[1]);
				histogram= new int[last_pos];
			}else if(l.startsWith("max")){
				String[] s = split(l, "\t");
				max_count = Integer.parseInt(s[1]);
			}else{
				String[] s = split(l, "\t");
				int index = Integer.parseInt(s[0]);
				int count = Integer.parseInt(s[1]);
				if(index > 0){
					histogram[index-1] = count;
				}
			}
		}
		//stage size
		int plot_height = round(pos_weight*last_pos);
		w = l_margin + r_margin + hist_width + lolly_width;
		h =  plot_height + _top_margin +_bottom_margin;


		h_rect = new Rectangle(l_margin, _top_margin, hist_width, plot_height);
		l_rect = new Rectangle(l_margin+hist_width, _top_margin, lolly_width, plot_height);

		//create LollypopData
		data = getLollypopData(gene_name);

		//assign positions to lollypop
		data.setPositions(l_rect, last_pos, lolly_size);
		// println("debug:"+data.toString());

		//check the height
		int last_y = data.pops.get(data.pops.size()-1).rect.y;
		if(last_y > h){
			h = last_y + _bottom_margin;
		}
		
	}

	public void setup(){
		size(w, h);

		//prepare PGraphics
		opaque = createGraphics(w, h);
		transparent = createGraphics(w, h);
		update_view(opaque, 255);
		update_view(transparent, 20);
		noLoop();
	}

	public void update_view (PGraphics pg, int opacity){
		pg.beginDraw();
		pg.smooth();
		pg.background(255);
		pg.textFont(small_font);
		int h_axis_x = h_rect.x+h_rect.width;

		//draw histogram
		pg.stroke(120);
		pg.strokeWeight(pos_weight);
		pg.fill(120);
		pg.textAlign(RIGHT, CENTER);
		for(int i = 0; i < last_pos; i++){
			int count = histogram[i];
			if(count > 0){
				int pos = i+1;
				float py = map(pos, 1, last_pos, h_rect.y, h_rect.y+h_rect.height);
				float plength = constrain(map(count, 0, 100, 0, h_rect.width), 0, h_rect.width);
				pg.line(h_axis_x, py, h_axis_x-plength, py);
				if(count >100){
					pg.text(count, h_axis_x-plength-2, py);
				}
			}
		}
		//lollypop
		if(data != null){
			for(Pop p: data.pops){
				//stick
				pg.stroke(0, opacity);
				pg.strokeWeight(1);
				pg.noFill();
				pg.beginShape();
				pg.vertex(p.sx, p.sy);
				pg.vertex(p.sx2, p.sy2);
				pg.vertex(p.sx3, p.sy3);
				pg.endShape();
				// line(l_rect.x, p.dy, l_rect.x+lolly_stick_length, p.dy);
				// ellipse(p.dx, p.dy, lolly_size, lolly_size);
				int num = p.mutations.size();
				float stripe_w = (float)lolly_size / (float)num;
				float left_x = (float)p.bx ;
				float runningX = left_x;
				for(Mutation m: p.mutations){
					pg.fill(colors[m.type_index], opacity);
					pg.noStroke();
					pg.rect(runningX, p.by, stripe_w, lolly_size);
					runningX += stripe_w; 
				}
				//out line
				pg.noFill();
				pg.stroke(0, opacity);
				pg.strokeWeight(1);
				pg.rect(left_x, p.by, lolly_size, lolly_size);

			}
		}


		//histogram grid
		pg.stroke(60);
		pg.strokeWeight(2);
		pg.line(h_axis_x, h_rect.y, h_axis_x, h_rect.y+h_rect.height);
		pg.fill(60);
		pg.textAlign(LEFT, CENTER);
		//grid
		for(int i = 1; i < last_pos+1; i++){
			if(i==1 || i%100 == 0){
				float gy = map(i, 1, last_pos, h_rect.y, h_rect.y+h_rect.height);
				pg.line(h_axis_x - 2,gy, h_axis_x+2, gy);
				// println("i="+i+"  gy="+gy);
				//label
				if(i==1 || i%200 == 0){
					pg.text(i, h_axis_x + 3, gy);
				}
			}
		}

		pg.endDraw();
	}

	public void draw(){
		background(255);
		textFont(small_font);
		int h_axis_x = h_rect.x+h_rect.width;

		if(selected_pop == null){
			image(opaque, 0, 0);
		}else{
			image(transparent, 0, 0);
			if(selected_pop != null){
				draw_active_pop();
			}
		}

		if(p_draw_counter > p_draw_max){
			println(" ---------  lollypop");
			noLoop();
			p_draw_counter = 0;
		}else{
			p_draw_counter ++;
		}
		
	}

	//draw selected pop
	public void draw_active_pop(){
		stroke(0);
		strokeWeight(1);
		noFill();
		beginShape();
		vertex(selected_pop.sx, selected_pop.sy);
		vertex(selected_pop.sx2, selected_pop.sy2);
		vertex(selected_pop.sx3, selected_pop.sy3);
		endShape();
		// line(l_rect.x, selected_pop.dy, l_rect.x+lolly_stick_length, selected_pop.dy);
		// ellipse(selected_pop.dx, selected_pop.dy, lolly_size, lolly_size);
		int num = selected_pop.mutations.size();
		float stripe_w = (float)lolly_size / (float)num;
		float left_x = (float)selected_pop.bx ;
		float runningX = left_x;
		for(Mutation m: selected_pop.mutations){
			fill(colors[m.type_index]);
			noStroke();
			rect(runningX, selected_pop.by, stripe_w, lolly_size);
			runningX += stripe_w; 
		}
		//out line
		noFill();
		stroke(0);
		strokeWeight(1);
		rect(left_x, selected_pop.by, lolly_size, lolly_size);

		//name
		textAlign(CENTER, BOTTOM);
		textFont(label_font);
		fill(0);
		String label = split(selected_pop.mut_name, "_")[1];
		text(label, selected_pop.bx, selected_pop.by);

	}


	public void mouseMoved(){
		cursor(ARROW);
		selected_pop = null;
		//check if it is hovering over any pop
		for(Pop p : data.pops){
			if(p.rect.contains(mouseX, mouseY)){
				cursor(HAND);
				selected_pop = p;
				// println("hovering: "+ p.mut_name);
				loop();

				update_main_view(p);

				// hovering_mutation = p.mutations.get(0);
				// parent.loop();

				return;
			}
		}
		loop();
		update_main_view(null);
	}
	public void mouseReleased(){}
	public void keyPressed(){}


}
